package azstack.com.optimime.clinic.ui.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.CategorySpinAdapter;
import azstack.com.optimime.clinic.ui.model.Category;
import azstack.com.optimime.clinic.ui.model.Clinic;
import azstack.com.optimime.clinic.ui.model.Result;
import azstack.com.optimime.clinic.ui.ui.ClinicInfoActivity;
import azstack.com.optimime.clinic.ui.utils.Constant;
import azstack.com.optimime.clinic.ui.utils.GPSTracker;
import azstack.com.optimime.clinic.ui.utils.HospitalRequestUtils;
import azstack.com.optimime.clinic.ui.utils.MapUtils;
import azstack.com.optimime.clinic.ui.utils.PreferenceUtil;
import azstack.com.optimime.clinic.ui.utils.Utils;
import azstack.com.optimime.clinic.ui.utils.VolleyUtils;

/**
 * Created by Dang Luu on 2/15/2017.
 */

public class MapFragment extends Fragment implements View.OnClickListener {
    private static final int REQUEST_LOCATION = 154;
    private static final String TAG = "MapFragment";
    private static final int KEY_SETTING_GPS = 15;
    private TextView tvDistance, tvDuration;

    private Spinner spinCategory;
    private int category_id = 0;
    private String category_name;

    MapView mapView;
    GoogleMap googleMap;
    GPSTracker gps = null;
    private LatLng sydney;

    private List<Clinic> clinics = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

        // Setting Dialog Title
        alertDialog.setTitle(R.string.title_gps);

        // Setting Dialog Message
        alertDialog.setMessage(getString(R.string.content_gps));

        // On pressing Settings button
        alertDialog.setPositiveButton(R.string.setting_gps, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, KEY_SETTING_GPS);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map, container, false);

        // Gets the MapView from the XML layout and creates it
        mapView = (MapView) v.findViewById(R.id.map);
        tvDistance = (TextView) v.findViewById(R.id.tvDistance);
        tvDuration = (TextView) v.findViewById(R.id.tvDuration);
        spinCategory = (Spinner) v.findViewById(R.id.spinCategory);
        mapView.onCreate(savedInstanceState);
        ((Button) v.findViewById(R.id.btnFind)).setOnClickListener(this);

        mapView.getMapAsync(
                new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap mMap) {
                        googleMap = mMap;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
                                                android.Manifest.permission.ACCESS_FINE_LOCATION},
                                        REQUEST_LOCATION);
                                return;
                            }
                        }
                        googleMap.setMyLocationEnabled(true);

                        if (Utils.hasLatLon(getContext())) {
                            gotoLocation(PreferenceUtil.getLat(getContext()), PreferenceUtil.getLon(getContext()));
                        } else {
                            if (!GPSTracker.checkGPSEnable(getContext())) {
                                showSettingsAlert();
                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(),
                                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
                                                android.Manifest.permission.ACCESS_FINE_LOCATION},
                                        REQUEST_LOCATION);
                                return;
                            }
                            gotoMyLocation();
                        }

                        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                            @Override
                            public void onInfoWindowClick(Marker marker) {
                                Intent intent = new Intent(getActivity(), ClinicInfoActivity.class);
                                for (Clinic clinic : clinics) {
                                    if (clinic.getName().equalsIgnoreCase(marker.getTitle())) {
                                        intent.putExtra(Constant.clinic_name, clinic);
                                        intent.putExtra(Constant.category_name, category_name);
                                        intent.putExtra(Constant.type, 1);
                                        startActivity(intent);
                                    }
                                }
                            }
                        });
                    }
                }
        );

        try {
            // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
            MapsInitializer.initialize(this.getActivity());
        } catch (Exception e) {
        }

        getListCategory();

        return v;
    }

    private void getListCategory() {
        HospitalRequestUtils.getListCategory(getContext(), PreferenceUtil.getToken(getContext()), new VolleyUtils.OnRequestListenner() {
            @Override
            public void onSussces(String response, Result result) {
                final List<Category> categories = VolleyUtils.getListModelFromRespone(response, Category.class);
                spinCategory.setAdapter(new CategorySpinAdapter(getContext(), categories));
                spinCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        category_id = categories.get(i).getId();
                        category_name = categories.get(i).getName();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == KEY_SETTING_GPS) {

            if (GPSTracker.checkGPSEnable(getContext())) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
                                        android.Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_LOCATION);
                        return;
                    }
                }
                gotoMyLocation();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            gotoMyLocation();
        } else {
            gotoMyLocation();
        }
    }

    public void gotoLocation(double latitude, double longitude) {
        sydney = new LatLng(latitude, longitude);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void gotoMyLocation() {
        gps = new GPSTracker(getActivity());

        if (gps.canGetLocation()) {
            final double longitude = gps.getLongitude();
            final double latitude = gps.getLatitude();

            HospitalRequestUtils.update(getContext(), PreferenceUtil.getToken(getContext()),
                    PreferenceUtil.getMyId(getContext()), latitude, longitude, new VolleyUtils.OnRequestListenner() {
                        @Override
                        public void onSussces(String response, Result result) {
                            if (result.isSuccess()) {
                                PreferenceUtil.setLat(getContext(), latitude);
                                PreferenceUtil.setlon(getContext(), longitude);
                            }
                        }

                        @Override
                        public void onError(String error) {

                        }
                    });

// For dropping a marker at a point on the Map
            sydney = new LatLng(latitude, longitude);
// For zooming automatically to the location of the marker
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            googleMap.setMyLocationEnabled(true);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }


    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    public void findPath(final double soureLat, final double sourceLon, final double disLat, final double disLon) {
        sydney = new LatLng(disLat, disLon);
        googleMap.addMarker(new MarkerOptions().position(sydney).title("Test")
                .snippet("Test Description")).showInfoWindow();
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {
                String url = MapUtils.makeURL(soureLat, sourceLon, disLat, disLon);
                MapUtils.JSONParser jParser = new MapUtils.JSONParser();
                String json = jParser.getJSONFromUrl(url);
                return json;
            }

            @Override
            protected void onPostExecute(String aVoid) {
                super.onPostExecute(aVoid);
                String[] results = MapUtils.drawPath(googleMap, aVoid);
                if (results != null && results.length >= 2) {
                    tvDistance.setText(results[0]);
                    tvDuration.setText(results[1]);
                }

            }
        }.execute();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnFind:
                double lat = PreferenceUtil.getLat(getContext()), lon = PreferenceUtil.getLon(getContext());
                if (category_id > 0) {
                    final ProgressDialog progressDialog = new ProgressDialog(getContext());
                    progressDialog.setMessage(getString(R.string.loading));
                    progressDialog.show();
                    HospitalRequestUtils.findClinic(getContext(), PreferenceUtil.getToken(getContext()),
                            lat, lon, category_id, new VolleyUtils.OnRequestListenner() {
                                @Override
                                public void onSussces(String response, Result result) {
                                    progressDialog.dismiss();
                                    // get near clinic
                                    clinics.clear();
                                    clinics = VolleyUtils.getListModelFromRespone(response, Clinic.class);

                                    if (googleMap != null) {
                                        googleMap.clear();
                                        if (!clinics.isEmpty()) {
                                            for (int i = 0; i < clinics.size(); i++) {
                                                final Clinic clinic = clinics.get(i);

                                                LatLng sydney = new LatLng(clinic.getLatitude(), clinic.getLongitude());
                                                googleMap.addMarker(new MarkerOptions().position(sydney).title(clinic.getName())
                                                        .snippet(clinic.getAddress())).showInfoWindow();
                                            }
                                            CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
                                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                        } else {
                                            Toast.makeText(getContext(), R.string.no_data, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }

                                @Override
                                public void onError(String error) {
                                    progressDialog.dismiss();
                                }
                            });
                    break;
                } else {
                    Toast.makeText(getContext(), R.string.please_select_category, Toast.LENGTH_SHORT).show();
                }
        }
    }
}
