package azstack.com.optimime.clinic.ui.ui;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.adapter.ListClinicAdapter;
import azstack.com.optimime.clinic.ui.model.Result;
import azstack.com.optimime.clinic.ui.model.Transaction;
import azstack.com.optimime.clinic.ui.utils.HospitalRequestUtils;
import azstack.com.optimime.clinic.ui.utils.PreferenceUtil;
import azstack.com.optimime.clinic.ui.utils.VolleyUtils;

/**
 * Created by VuVan on 07/03/2017.
 */

public class HistoryOrderActivity extends BaseActivityHasBackButton {

    private RecyclerView recyclerView, recyclerSearch;
    private TextView tvResult;
    private SearchView searchView;
    private LinearLayout viewLoadMore;
    private LinearLayoutManager layoutManager;
    private ListClinicAdapter transactionAdapter, transactionAdapterSearch;

    private List<Transaction> transactions = new ArrayList<>();
    private List<Transaction> searchTransactions = new ArrayList<>();

    private int mTotal = 0;
    private int page = 0;
    private int visibleItemCount;
    private int totalItemCount;
    private int pastVisibleItems;
    private int firstVisibleItems;
    private boolean isLoadMore = false;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        initControl();
        initListener();
        handleIntent(getIntent());
    }

    private void initControl() {
        getSupportActionBar().setTitle(R.string.history);
        recyclerView = (RecyclerView) findViewById(R.id.listTransaction);
        recyclerSearch = (RecyclerView) findViewById(R.id.listSearch);
        tvResult = (TextView) findViewById(R.id.tvResult);
        viewLoadMore = (LinearLayout) findViewById(R.id.v_loadmore);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        getListTransaction(++page);
    }

    private void initListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItems = layoutManager.findFirstVisibleItemPosition();
                if (firstVisibleItems > 0) {
                    pastVisibleItems = firstVisibleItems;
                }

                if (pastVisibleItems > 0 && (pastVisibleItems + visibleItemCount) >= totalItemCount) {
                    if (!isLoadMore && (transactions.size() < mTotal)) {
                        isLoadMore = true;
                        viewLoadMore.setVisibility(View.VISIBLE);
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getListTransaction(++page);
                            }
                        }, 1000);
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        menu.findItem(R.id.action_today).setVisible(false);
        menu.findItem(R.id.action_logout).setVisible(false);
        menu.findItem(R.id.search).setVisible(true);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        MenuItemCompat.setOnActionExpandListener(menu.findItem(R.id.search), new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                //The SearchView is opening
                Log.e("opening", "opening");
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                //The SearchView is closing. Do stuff here.
                searchTransactions.clear();
                recyclerSearch.setVisibility(View.GONE);
                if (transactions.isEmpty()) {
                    recyclerView.setVisibility(View.GONE);
                    tvResult.setVisibility(View.VISIBLE);
                    tvResult.setText(R.string.no_history);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    tvResult.setVisibility(View.GONE);
                }
                return true;
            }
        });

        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // handles a search query
            searchView.setFocusable(false);
            this.getCurrentFocus().clearFocus();
            String query = intent.getStringExtra(SearchManager.QUERY);
            getListSearchTransaction(query);
        }
    }

    private void getListTransaction(final int page) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        if (page == 1) {
            progressDialog.show();
        }
        HospitalRequestUtils.getListTransaction(this, PreferenceUtil.getToken(this), PreferenceUtil.getMyId(this), page,
                new VolleyUtils.OnRequestListenner() {
                    @Override
                    public void onSussces(String response, Result result) {
                        if (page == 1) {
                            progressDialog.dismiss();
                        }
                        viewLoadMore.setVisibility(View.GONE);
                        if (result.isSuccess()) {
                            mTotal = result.getTotal();
                            List<Transaction> lstTransaction = VolleyUtils.getListTransactionResponse(response);
                            transactions.addAll(lstTransaction);
                            if (!transactions.isEmpty()) {
                                tvResult.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                if (transactionAdapter == null) {
                                    transactionAdapter = new ListClinicAdapter(HistoryOrderActivity.this, transactions, 1);
//                                    recyclerView.setLayoutManager(new LinearLayoutManager(HistoryOrderActivity.this));
                                    recyclerView.setAdapter(transactionAdapter);
                                } else {
                                    isLoadMore = false;
                                    transactionAdapter.setList(transactions);
                                }
                            } else {
                                tvResult.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            }
                        } else {
                            tvResult.setVisibility(View.VISIBLE);
                            tvResult.setText(result.getMessage());
                        }
                    }

                    @Override
                    public void onError(String error) {
                        progressDialog.dismiss();
                        Log.e("onError", error);
                    }
                });
    }

    private void getListSearchTransaction(String search) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        HospitalRequestUtils.getListSearchTransaction(this, PreferenceUtil.getToken(this), search,
                new VolleyUtils.OnRequestListenner() {
                    @Override
                    public void onSussces(String response, Result result) {
                        progressDialog.dismiss();
                        if (result.isSuccess()) {
                            searchTransactions = VolleyUtils.getListModelFromRespone(response, Transaction.class);
                            if (!searchTransactions.isEmpty()) {
                                tvResult.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                                recyclerSearch.setVisibility(View.VISIBLE);
                                transactionAdapterSearch = new ListClinicAdapter(HistoryOrderActivity.this, searchTransactions, 1);
                                recyclerSearch.setLayoutManager(new LinearLayoutManager(HistoryOrderActivity.this));
                                recyclerSearch.setAdapter(transactionAdapterSearch);
                            } else {
                                tvResult.setVisibility(View.VISIBLE);
                                tvResult.setText(R.string.no_history);
                                recyclerView.setVisibility(View.GONE);
                                recyclerSearch.setVisibility(View.GONE);
                            }
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            recyclerSearch.setVisibility(View.GONE);
                            tvResult.setVisibility(View.VISIBLE);
                            tvResult.setText(result.getMessage());
                        }
                    }

                    @Override
                    public void onError(String error) {
                        progressDialog.dismiss();
                        Log.e("onError", error);
                    }
                });
    }
}
