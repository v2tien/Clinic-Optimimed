package azstack.com.optimime.clinic.ui.ui;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.alamkanak.weekview.WeekViewEvent;
import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.model.CalendarHospital;
import azstack.com.optimime.clinic.ui.model.Clinic;
import azstack.com.optimime.clinic.ui.model.Patient;
import azstack.com.optimime.clinic.ui.model.Transaction;
import azstack.com.optimime.clinic.ui.utils.Constant;
import azstack.com.optimime.clinic.ui.utils.PreferenceUtil;
import azstack.com.optimime.clinic.ui.utils.Utils;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

/**
 * Created by VuVan on 23/03/2017.
 */

public class BookingActivity extends BaseActivityHasBackButton {

    private static final int PICK_IMAGE_CHOOSER_REQUEST_CODE = 12;
    private static final int PICK_IMAGE_INVOICE_CHOOSER_REQUEST_CODE = 13;
    private static final int REQUEST_CAPTURE_PHOTO = 11;
    private static final int REQUEST_CAPTURE_PHOTO_INVOICE = 10;
    private static final int REQUEST_PERMISSION_CAMERA = 34;

    private EditText edtPatientName, edtContactNumber, edtDepartment, edtConsultant, edtMRI, edtNRIC, edtRemark;
    private ImageView imvCamera, imvPhoto, imvRemove;
    private ImageView imvCameraInvoice, imvPhotoInvoice, imvRemoveInvoice;
    private Button btnConfirm;
    private RelativeLayout viewPhoto, viewInvoice;

    private String base64Image, base64Invoice, bookingTime;
    private String pathFilePicture, pathFileInvoice;
    private Bitmap bitmap, bitmapInvoice;
    private WeekViewEvent event;
    private CalendarHospital calendarHospital;
    private Transaction transaction = new Transaction();
    private Patient patient = new Patient();
    private Clinic clinic = new Clinic();
    private int type, mode, status;
    private long transactionId, patientId;
    private boolean isChangeForm = false, isChangeInvoice = false;
    String[] permissions = new String[]{READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA,};
    public static final int MULTIPLE_PERMISSIONS = 15, MULTIPLE_PERMISSIONS_2 = 16;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);

        initComponent();

        mode = getIntent().getIntExtra(Constant.MODE, Constant.FROM_CALENDAR);
        if (mode == Constant.FROM_CALENDAR) {
            type = getIntent().getIntExtra(Constant.CHOOSE_TYPE, 1);
            event = getIntent().getParcelableExtra(Constant.CLINIC);
            calendarHospital = getIntent().getParcelableExtra(Constant.CALENDAR);
        } else {
            transaction = getIntent().getParcelableExtra(Constant.TRANSACTION);
            fillData();
        }
    }

    private void initComponent() {
        getSupportActionBar().setTitle(R.string.lbl_booking);

        btnConfirm = (Button) findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(this);
        edtPatientName = (EditText) findViewById(R.id.edtPatientName);
        edtContactNumber = (EditText) findViewById(R.id.edtContactNumber);
        edtDepartment = (EditText) findViewById(R.id.edtDepartment);
        edtConsultant = (EditText) findViewById(R.id.edtConsultant);
        edtNRIC = (EditText) findViewById(R.id.edtNRIC);
        edtMRI = (EditText) findViewById(R.id.edtMRI);
        edtRemark = (EditText) findViewById(R.id.edtRemark);
        imvCamera = (ImageView) findViewById(R.id.imvCamera);
        imvCamera.setOnClickListener(this);
        imvPhoto = (ImageView) findViewById(R.id.imvPhoto);
        imvRemove = (ImageView) findViewById(R.id.imvRemove);
        imvRemove.setOnClickListener(this);
        viewPhoto = (RelativeLayout) findViewById(R.id.viewPhoto);
        viewInvoice = (RelativeLayout) findViewById(R.id.viewPhotoInvoice);
        imvCameraInvoice = (ImageView) findViewById(R.id.imvCameraInvoice);
        imvCameraInvoice.setOnClickListener(this);
        imvPhotoInvoice = (ImageView) findViewById(R.id.imvPhotoInvoice);
        imvRemoveInvoice = (ImageView) findViewById(R.id.imvRemoveInvoice);
        imvRemoveInvoice.setOnClickListener(this);
    }

    private void fillData() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        type = transaction.getType();
        clinic = transaction.getClinic();
        bookingTime = transaction.getBooking_time();
        status = transaction.getStatus();
        transactionId = transaction.getId();
        patientId = transaction.getPatient_id();

        edtPatientName.setText(transaction.getPatient().getFullname());
        edtNRIC.setText(transaction.getPatient().getIdentify_card());
        edtContactNumber.setText(transaction.getPatient().getPhone());
        edtDepartment.setText(transaction.getHospital_department());
        edtConsultant.setText(transaction.getConsultant_name());
        edtMRI.setText(transaction.getMri_cost());
        edtRemark.setText(transaction.getNote());
        edtPatientName.setSelection(transaction.getPatient().getFullname().length());

        pathFilePicture = transaction.getPatient().getScanned_profile();
        pathFileInvoice = transaction.getScanned_invoice();

        if (pathFilePicture == null || pathFilePicture.isEmpty()) {
            viewPhoto.setVisibility(View.INVISIBLE);
            imvRemove.setVisibility(View.GONE);
            imvCamera.setVisibility(View.VISIBLE);
        }else{
            Glide.with(this).load(pathFilePicture).into(imvPhoto);
            PreferenceUtil.setBase64Image(this, pathFilePicture);
            viewPhoto.setVisibility(View.VISIBLE);
            imvRemove.setVisibility(View.VISIBLE);
            imvCamera.setVisibility(View.GONE);
        }
        Glide.with(this).load(pathFileInvoice).into(imvPhotoInvoice);
        PreferenceUtil.setBase64Invoice(this, pathFileInvoice);
        viewInvoice.setVisibility(View.VISIBLE);
        imvRemoveInvoice.setVisibility(View.VISIBLE);
        imvCameraInvoice.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btnConfirm:
                if (edtPatientName.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please enter patient name", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edtNRIC.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please enter NRIC/FIN", Toast.LENGTH_SHORT).show();
                    return;
                }
                String text = edtNRIC.getText().toString();
                String first = text.substring(0, 1);
                String last = text.substring(text.length() - 1, text.length());
                String center = text.substring(1, text.length() - 1);
                if (!Utils.isValidNRIC(first) || !Utils.isValidNRIC(last) || Utils.isValidNumber(center)) {
                    Toast.makeText(this, "Invalid format NRIC/FIN", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (edtMRI.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please enter MRI cost", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (edtContactNumber.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please enter contact number", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (pathFileInvoice == null || pathFileInvoice.isEmpty()) {
                    Toast.makeText(this, "Please upload invoice", Toast.LENGTH_SHORT).show();
                    return;
                }
                patient.setFullname(edtPatientName.getText().toString());
                patient.setIdentify_card(edtNRIC.getText().toString());
                patient.setPhone(edtContactNumber.getText().toString());
                transaction.setPatient(patient);
                transaction.setHospital_department(edtDepartment.getText().toString());
                transaction.setConsultant_name(edtConsultant.getText().toString());
                transaction.setMri_cost(edtMRI.getText().toString());
                transaction.setNote(edtRemark.getText().toString());
                transaction.setType(type);
                transaction.setFilePathForm(pathFilePicture);
                transaction.setFilePathInvoice(pathFileInvoice);

                Intent intent = new Intent(BookingActivity.this, ConfirmBookingActivity.class);
                if (mode == Constant.FROM_CALENDAR) {
                    transaction.setStatus(1);
                    transaction.setPatient_id(0);
                    transaction.setId(0);
                    transaction.setBooking_time(PreferenceUtil.getTimeKey(getBaseContext()));
                    intent.putExtra(Constant.CLINIC, event);
                    intent.putExtra(Constant.CALENDAR, calendarHospital);
                    intent.putExtra(Constant.MODE, Constant.FROM_CALENDAR);
                } else {
                    transaction.setId(transactionId);
                    transaction.setPatient_id(patientId);
                    transaction.setBooking_time(bookingTime);
                    transaction.setStatus(status);
                    intent.putExtra(Constant.CLINIC, clinic);
                    intent.putExtra(Constant.MODE, Constant.FROM_HISTORY);
                }
                intent.putExtra(Constant.CHANGE_FORM, isChangeForm);
                intent.putExtra(Constant.CHANGE_INVOICE, isChangeInvoice);
                intent.putExtra(Constant.TRANSACTION, transaction);
                startActivity(intent);
                break;
            case R.id.imvRemove:
                viewPhoto.setVisibility(view.INVISIBLE);
                imvRemove.setVisibility(view.GONE);
                imvCamera.setVisibility(view.VISIBLE);
                pathFilePicture = "";
                PreferenceUtil.setBase64Image(this, "");
                isChangeForm = false;
                break;
            case R.id.imvCamera:
                Utils.hideKeyboard(BookingActivity.this);

                String[] actions = getResources().getStringArray(R.array.list_action_add_picture);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setItems(actions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int select) {
                        if (select == 0) {
                            if (Utils.checkPermissionReadExternalStorage(BookingActivity.this)) {
                                Utils.galleryIntentGallery(BookingActivity.this, PICK_IMAGE_CHOOSER_REQUEST_CODE);
                            }
                        } else if (select == 1) {
                            if (checkPermissions(MULTIPLE_PERMISSIONS)) {
                                openCamera();
                            }
//                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                                List<String> lstPermissions = new ArrayList<>();
//
//                                if (ContextCompat.checkSelfPermission(BookingActivity.this,
//                                        READ_EXTERNAL_STORAGE)
//                                        != PackageManager.PERMISSION_GRANTED) {
//                                    lstPermissions.add(READ_EXTERNAL_STORAGE);
//                                }
//
//                                if (ContextCompat.checkSelfPermission(BookingActivity.this,
//                                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                                        != PackageManager.PERMISSION_GRANTED) {
//                                    lstPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
//                                }
//
//                                if (ContextCompat.checkSelfPermission(BookingActivity.this,
//                                        Manifest.permission.CAMERA)
//                                        != PackageManager.PERMISSION_GRANTED) {
//                                    lstPermissions.add(Manifest.permission.CAMERA);
//                                }
//
//                                if (lstPermissions.size() == 0) {
//                                    openCamera();
//                                } else {
//                                    String[] permissions = new String[lstPermissions.size()];
//                                    for (int i = 0; i < lstPermissions.size(); i++) {
//                                        permissions[i] = lstPermissions.get(i);
//                                    }
//                                    ActivityCompat.requestPermissions(BookingActivity.this, permissions, REQUEST_PERMISSION_CAMERA);
//                                }
//                            } else {
//                                openCamera();
//                            }
                        }
                    }
                });
                builder.show();
                break;

            case R.id.imvCameraInvoice:
                Utils.hideKeyboard(BookingActivity.this);

                String[] action = getResources().getStringArray(R.array.list_action_add_picture);
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setItems(action, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int select) {
                        if (select == 0) {
                            if (Utils.checkPermissionReadExternalStorage(BookingActivity.this)) {
                                Utils.galleryIntentGallery(BookingActivity.this, PICK_IMAGE_INVOICE_CHOOSER_REQUEST_CODE);
                            }
                        } else if (select == 1) {
                            if (checkPermissions(MULTIPLE_PERMISSIONS_2)) {
                                openCameraInvoice();
                            }
//                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                                List<String> lstPermissions = new ArrayList<>();
//
//                                if (ContextCompat.checkSelfPermission(BookingActivity.this,
//                                        READ_EXTERNAL_STORAGE)
//                                        != PackageManager.PERMISSION_GRANTED) {
//                                    lstPermissions.add(READ_EXTERNAL_STORAGE);
//                                }
//
//                                if (ContextCompat.checkSelfPermission(BookingActivity.this,
//                                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                                        != PackageManager.PERMISSION_GRANTED) {
//                                    lstPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
//                                }
//
//                                if (ContextCompat.checkSelfPermission(BookingActivity.this,
//                                        Manifest.permission.CAMERA)
//                                        != PackageManager.PERMISSION_GRANTED) {
//                                    lstPermissions.add(Manifest.permission.CAMERA);
//                                }
//
//                                if (lstPermissions.size() == 0) {
//                                    openCameraInvoice();
//                                } else {
//                                    String[] permissions = new String[lstPermissions.size()];
//                                    for (int i = 0; i < lstPermissions.size(); i++) {
//                                        permissions[i] = lstPermissions.get(i);
//                                    }
//                                    ActivityCompat.requestPermissions(BookingActivity.this, permissions, REQUEST_PERMISSION_CAMERA);
//                                }
//                            } else {
//                                openCameraInvoice();
//                            }
                        }
                    }
                });
                dialog.show();
                break;

            case R.id.imvRemoveInvoice:
                viewInvoice.setVisibility(view.INVISIBLE);
                imvRemoveInvoice.setVisibility(view.GONE);
                imvCameraInvoice.setVisibility(view.VISIBLE);
                pathFileInvoice = "";
                PreferenceUtil.setBase64Invoice(this, "");
                isChangeInvoice = false;
                break;
        }
    }

    public void openCamera() {
        File file = new File(Environment.getExternalStorageDirectory() + "/" + Constant.FOLDER_NAME + "/" + Constant.FOLDER_NAME_IMAGE);
        if (!file.exists()) {
            file.mkdirs();
        }
        File fileImg = new File(file, System.currentTimeMillis() + ".jpg");
        pathFilePicture = fileImg.getPath();
        Uri outputFileUri = FileProvider.getUriForFile(getApplicationContext(),
                getApplicationContext().getApplicationContext().getPackageName() + ".provider", fileImg);
        Intent intent3 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent3.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        intent3.putExtra(Intent.EXTRA_RETURN_RESULT, true);
        intent3.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent3, REQUEST_CAPTURE_PHOTO);
    }

    public void openCameraInvoice() {
        File file = new File(Environment.getExternalStorageDirectory() + "/" + Constant.FOLDER_NAME + "/" + Constant.FOLDER_NAME_IMAGE);
        if (!file.exists()) {
            file.mkdirs();
        }
        File fileImg = new File(file, System.currentTimeMillis() + ".jpg");
        pathFileInvoice = fileImg.getPath();
        Uri outputFileUri = FileProvider.getUriForFile(getApplicationContext(),
                getApplicationContext().getApplicationContext().getPackageName() + ".provider", fileImg);
        Intent intent3 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent3.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        intent3.putExtra(Intent.EXTRA_RETURN_RESULT, true);
        intent3.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent3, REQUEST_CAPTURE_PHOTO_INVOICE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_CHOOSER_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Glide.with(this).load(data.getData()).into(imvPhoto);
                viewPhoto.setVisibility(View.VISIBLE);
                imvRemove.setVisibility(View.VISIBLE);
                imvCamera.setVisibility(View.GONE);
                isChangeForm = true;

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                pathFilePicture = cursor.getString(columnIndex);
                cursor.close();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                    base64Image = Utils.encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 50);
                    PreferenceUtil.setBase64Image(this, base64Image);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (requestCode == REQUEST_CAPTURE_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Glide.with(this).load(pathFilePicture).into(imvPhoto);
                    viewPhoto.setVisibility(View.VISIBLE);
                    imvRemove.setVisibility(View.VISIBLE);
                    imvCamera.setVisibility(View.GONE);
                    isChangeForm = true;
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    bitmap = BitmapFactory.decodeFile(pathFilePicture, options);
                    base64Image = Utils.encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 50);
                    PreferenceUtil.setBase64Image(this, base64Image);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (requestCode == PICK_IMAGE_INVOICE_CHOOSER_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Glide.with(this).load(data.getData()).into(imvPhotoInvoice);
                viewInvoice.setVisibility(View.VISIBLE);
                imvRemoveInvoice.setVisibility(View.VISIBLE);
                imvCameraInvoice.setVisibility(View.GONE);
                isChangeInvoice = true;

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                pathFileInvoice = cursor.getString(columnIndex);
                cursor.close();
                try {
                    bitmapInvoice = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                    base64Invoice = Utils.encodeToBase64(bitmapInvoice, Bitmap.CompressFormat.JPEG, 50);
                    PreferenceUtil.setBase64Invoice(this, base64Invoice);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (requestCode == REQUEST_CAPTURE_PHOTO_INVOICE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Glide.with(this).load(pathFileInvoice).into(imvPhotoInvoice);
                    viewInvoice.setVisibility(View.VISIBLE);
                    imvRemoveInvoice.setVisibility(View.VISIBLE);
                    imvCameraInvoice.setVisibility(View.GONE);
                    isChangeInvoice = true;
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    bitmapInvoice = BitmapFactory.decodeFile(pathFileInvoice, options);
                    base64Invoice = Utils.encodeToBase64(bitmapInvoice, Bitmap.CompressFormat.JPEG, 50);
                    PreferenceUtil.setBase64Invoice(this, base64Invoice);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
//                    callgalary();
                openCamera();
                return;

            case MULTIPLE_PERMISSIONS_2: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    callCamera();
                    openCameraInvoice();
                }
            }
        }
    }

    // check Permissions select image
    private boolean checkPermissions(int mun) {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(getApplicationContext(), p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), mun);
            return false;
        }
        return true;
    }
}
