package azstack.com.optimime.clinic.ui.ui;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.model.Clinic;
import azstack.com.optimime.clinic.ui.model.Result;
import azstack.com.optimime.clinic.ui.model.Transaction;
import azstack.com.optimime.clinic.ui.utils.Constant;
import azstack.com.optimime.clinic.ui.utils.HospitalRequestUtils;
import azstack.com.optimime.clinic.ui.utils.PreferenceUtil;
import azstack.com.optimime.clinic.ui.utils.Utils;
import azstack.com.optimime.clinic.ui.utils.VolleyUtils;

public class ClinicInfoActivity extends BaseActivityHasBackButton {

    private static final int PICK_IMAGE_CHOOSER_REQUEST_CODE = 12;
    private static final int REQUEST_CAPTURE_PHOTO = 11;
    private static final int REQUEST_PERMISSION_CAMERA = 34;
    private ImageView imgPicture, imvPhoto;
    private LinearLayout viewName, viewCategory;
    private EditText edtFullname, edtAddress, edtDescription, edtDepartment, edtConsultant;
    private EditText edtPhone, edtICCard;
    private RelativeLayout viewAddImage;
    private TextView tvName, tvCategory;
    private Button btnOrder;
    private String base64Image;
    private String pathFilePicture;
    private String nameCategory, nameClinic;
    private int id, type;
    private Clinic clinic;
    private Bitmap bitmap;
    private Transaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_info);

        initComponent();
        type = getIntent().getIntExtra(Constant.type, 0);
        if (type == 1) {
            getSupportActionBar().setTitle(R.string.clinic_info);
            if (getIntent().hasExtra(Constant.clinic_name)) {
                try {
                    clinic = getIntent().getParcelableExtra(Constant.clinic_name);
                    nameCategory = getIntent().getStringExtra(Constant.category_name);
                    tvName.setText(clinic.getName());
                    tvCategory.setText(nameCategory);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (type == 2) {
            getSupportActionBar().setTitle(R.string.patient_info);
            transaction = getIntent().getParcelableExtra(Constant.transaction);
            fillDataPatient();
        }
    }

    private void initComponent() {
        tvName = (TextView) findViewById(R.id.tvName);
        tvCategory = (TextView) findViewById(R.id.tvCategory);
        imgPicture = (ImageView) findViewById(R.id.imgPicture);
        imvPhoto = (ImageView) findViewById(R.id.imvPhoto);
        edtFullname = (EditText) findViewById(R.id.edtFullname);
        edtAddress = (EditText) findViewById(R.id.edtAddress);
        edtDescription = (EditText) findViewById(R.id.edtDescription);
        edtPhone = (EditText) findViewById(R.id.edtPhone);
        edtICCard = (EditText) findViewById(R.id.edtIDCard);
        edtDepartment = (EditText) findViewById(R.id.edtDepartment);
        edtConsultant = (EditText) findViewById(R.id.edtConsultant);
        viewAddImage = (RelativeLayout) findViewById(R.id.btnAddImage);
        viewName = (LinearLayout) findViewById(R.id.viewName);
        viewCategory = (LinearLayout) findViewById(R.id.viewCategory);
        viewAddImage.setOnClickListener(this);
        btnOrder = (Button) findViewById(R.id.btnSend);
        btnOrder.setOnClickListener(this);
    }

    private void fillDataPatient() {
        viewName.setVisibility(View.GONE);
        viewCategory.setVisibility(View.GONE);
        viewAddImage.setVisibility(View.GONE);
        btnOrder.setVisibility(View.GONE);
        imvPhoto.setVisibility(View.VISIBLE);
        edtFullname.setText(transaction.getPatient().getFullname());
        edtAddress.setText(transaction.getPatient().getAddress());
        edtDescription.setText(transaction.getPatient().getDescription());
        edtDepartment.setText(transaction.getHospital_department());
        edtConsultant.setText(transaction.getConsultant_name());
        edtPhone.setText(transaction.getPatient().getPhone());
        edtICCard.setText(transaction.getPatient().getIdentify_card());
        edtFullname.setEnabled(false);
        edtAddress.setEnabled(false);
        edtDescription.setEnabled(false);
        edtDepartment.setEnabled(false);
        edtConsultant.setEnabled(false);
        edtPhone.setEnabled(false);
        edtICCard.setEnabled(false);
        Glide.with(this).load(transaction.getPatient().getScanned_profile()).into(imvPhoto);
    }

    public void getInfo(int id) {
        HospitalRequestUtils.getClinnicInfo(this, PreferenceUtil.getToken(this), id, new VolleyUtils.OnRequestListenner() {
            @Override
            public void onSussces(String response, Result result) {
                if (result.isSuccess()) {
                    Clinic clinic = (Clinic) VolleyUtils.getObject(response, Clinic.class);
                    tvName.setText(clinic.getName());
                    tvCategory.setText(nameCategory);
                } else {
                    Toast.makeText(ClinicInfoActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    public void openCamera() {
        File file = new File(Environment.getExternalStorageDirectory() + "/" + Constant.FOLDER_NAME + "/" + Constant.FOLDER_NAME_IMAGE);
        if (!file.exists()) {
            file.mkdirs();
        }
        File fileImg = new File(file, System.currentTimeMillis() + ".jpg");
        pathFilePicture = fileImg.getPath();
        Intent intent3 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent3.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileImg));
        startActivityForResult(intent3, REQUEST_CAPTURE_PHOTO);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btnAddImage:
                String[] actions = getResources().getStringArray(R.array.list_action_add_picture);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setItems(actions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int select) {
                        if (select == 0) {
                            if (Utils.checkPermissionReadExternalStorage(ClinicInfoActivity.this)) {
                                Utils.galleryIntentGallery(ClinicInfoActivity.this, PICK_IMAGE_CHOOSER_REQUEST_CODE);
                            }
                        } else if (select == 1) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                List<String> lstPermissions = new ArrayList<>();

                                if (ContextCompat.checkSelfPermission(ClinicInfoActivity.this,
                                        Manifest.permission.READ_EXTERNAL_STORAGE)
                                        != PackageManager.PERMISSION_GRANTED) {
                                    lstPermissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                                }

                                if (ContextCompat.checkSelfPermission(ClinicInfoActivity.this,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                        != PackageManager.PERMISSION_GRANTED) {
                                    lstPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                                }

                                if (ContextCompat.checkSelfPermission(ClinicInfoActivity.this,
                                        Manifest.permission.CAMERA)
                                        != PackageManager.PERMISSION_GRANTED) {
                                    lstPermissions.add(Manifest.permission.CAMERA);
                                }

                                if (lstPermissions.size() == 0) {
                                    openCamera();
                                } else {
                                    String[] permissions = new String[lstPermissions.size()];
                                    for (int i = 0; i < lstPermissions.size(); i++) {
                                        permissions[i] = lstPermissions.get(i);
                                    }
                                    ActivityCompat.requestPermissions(ClinicInfoActivity.this, permissions, REQUEST_PERMISSION_CAMERA);
                                }
                            } else {
                                openCamera();
                            }
                        }
                    }
                });
                builder.show();
                break;
            case R.id.btnSend:
                String fullName = edtFullname.getText().toString();
                String address = edtAddress.getText().toString();
                String description = edtDescription.getText().toString();
                String phone = edtPhone.getText().toString();
                String icCard = edtICCard.getText().toString();
                final String department = edtDepartment.getText().toString();
                final String consultant = edtConsultant.getText().toString();

                if (fullName.isEmpty()) {
                    Toast.makeText(this, R.string.invalid_info, Toast.LENGTH_SHORT).show();
                    return;
                } else if (bitmap == null) {
                    Toast.makeText(this, R.string.invalid_image, Toast.LENGTH_SHORT).show();
                    return;
                }

                base64Image = Utils.encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 50);

                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setMessage(getString(R.string.loading));
                progressDialog.show();
//                HospitalRequestUtils.creatPatient(this, PreferenceUtil.getToken(this), fullName, address,
//                        description, base64Image, "jpg", 1 + "", phone, icCard, new VolleyUtils.OnRequestListenner() {
//                            @Override
//                            public void onSussces(String response, Result result) {
//                                progressDialog.dismiss();
//                                if (result.isSuccess()) {
//                                    int patientId = -1;
//                                    int status = 1;
//                                    try {
//                                        JSONObject jsonObject = new JSONObject(response);
//                                        patientId = jsonObject.getInt(Constant.id);
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                    }

//                                    HospitalRequestUtils.transactionpatient(ClinicInfoActivity.this, PreferenceUtil.getToken(ClinicInfoActivity.this),
//                                            patientId, clinic.getId(), PreferenceUtil.getUserId(ClinicInfoActivity.this), status,
//                                            department, consultant, new VolleyUtils.OnRequestListenner() {
//                                                @Override
//                                                public void onSussces(String response, Result result) {
//                                                    if (result.isSuccess()) {
//                                                        finish();
//                                                        Toast.makeText(ClinicInfoActivity.this, R.string.successfully, Toast.LENGTH_SHORT).show();
//                                                    }
//                                                }
//
//                                                @Override
//                                                public void onError(String error) {
//
//                                                }
//                                            });
//                                } else
//                                    Toast.makeText(ClinicInfoActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//
//                            @Override
//                            public void onError(String error) {
//                                progressDialog.dismiss();
//                            }
//                        });
                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CAMERA) {
            if (Utils.checkPermission(grantResults))
                openCamera();
        }
        if (requestCode == Utils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (Utils.checkPermission(grantResults)) {
                Utils.galleryIntentGallery(this, PICK_IMAGE_CHOOSER_REQUEST_CODE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_CHOOSER_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Glide.with(this).load(data.getData()).into(imgPicture);
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (requestCode == REQUEST_CAPTURE_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                try {
//                imgPicture.setImageURI(Uri.parse(pathFilePicture));
                    Glide.with(this).load(pathFilePicture).into(imgPicture);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    bitmap = BitmapFactory.decodeFile(pathFilePicture, options);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
