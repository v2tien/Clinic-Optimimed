package azstack.com.optimime.clinic.ui.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alamkanak.weekview.WeekViewEvent;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.model.CalendarHospital;
import azstack.com.optimime.clinic.ui.model.Clinic;
import azstack.com.optimime.clinic.ui.model.Transaction;
import azstack.com.optimime.clinic.ui.utils.Constant;
import azstack.com.optimime.clinic.ui.utils.PreferenceUtil;
import azstack.com.optimime.clinic.ui.utils.Utils;

/**
 * Created by VuVan on 25/03/2017.
 */

public class ConfirmBookingActivity extends BaseActivityHasBackButton {

    private Button btnShare, btnPrint;
    private TextView tvBookingType, tvFor, tvOn, tvAt, tvAddress, tvHour, tvForm;
    private MapView mMap;
    private RelativeLayout viewForm;
    private ImageView imvPhoto, imvInvoice;

    private GoogleMap mGoogleMap;
    private int type, mode;
    private Transaction transaction;
    private Clinic clinic;
    private WeekViewEvent event;
    private CalendarHospital calendarHospital;
    private String patientName, contact, department, consultant, nric, mri, remark;
    private boolean isChangeForm = false, isChangeInvoice = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_booking);
        initComponent();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mode = extras.getInt(Constant.MODE, Constant.FROM_CALENDAR);
            transaction = extras.getParcelable(Constant.TRANSACTION);
            isChangeForm = extras.getBoolean(Constant.CHANGE_FORM, false);
            isChangeInvoice = extras.getBoolean(Constant.CHANGE_INVOICE, false);
            if (mode == Constant.FROM_CALENDAR) {
                event = extras.getParcelable(Constant.CLINIC);
                calendarHospital = extras.getParcelable(Constant.CALENDAR);
            } else {
                clinic = extras.getParcelable(Constant.CLINIC);
            }
        }

        mMap.onCreate(savedInstanceState);

        fillData();
    }

    private void initComponent() {
        getSupportActionBar().setTitle(R.string.lbl_confirm_booking);
        btnShare = (Button) findViewById(R.id.btnShare);
        btnShare.setOnClickListener(this);
        btnPrint = (Button) findViewById(R.id.btnPrint);
        btnPrint.setOnClickListener(this);

        tvBookingType = (TextView) findViewById(R.id.tvBookingType);
        tvFor = (TextView) findViewById(R.id.tvFor);
        tvOn = (TextView) findViewById(R.id.tvOn);
        tvAt = (TextView) findViewById(R.id.tvAt);
        tvAddress = (TextView) findViewById(R.id.tvAddress);
        mMap = (MapView) findViewById(R.id.map_view);
        imvPhoto = (ImageView) findViewById(R.id.imvPhoto);
        imvInvoice = (ImageView) findViewById(R.id.imvInvoice);
        tvHour = (TextView) findViewById(R.id.tvHour);
        tvForm = (TextView) findViewById(R.id.tvOrderForm);
        viewForm = (RelativeLayout)findViewById(R.id.viewPhoto);
    }

    private void fillData() {
        // set type booking
        type = transaction.getType();
        patientName = transaction.getPatient().getFullname();
        contact = transaction.getPatient().getPhone();
        department = transaction.getHospital_department();
        consultant = transaction.getConsultant_name();
        nric = transaction.getPatient().getIdentify_card();
        mri = transaction.getMri_cost();
        remark = transaction.getNote();

        if (type == 1) {
            tvBookingType.setText(getString(R.string.lbl_closed) + " appointment");
        } else if (type == 2) {
            tvBookingType.setText(getString(R.string.lbl_open) + " appointment");
        } else if (type == 3) {
            tvBookingType.setText(getString(R.string.lbl_other) + " appointment");
        }

        // photo
        if(transaction.getFilePathForm() == null || transaction.getFilePathForm().isEmpty()) {
            viewForm.setVisibility(View.GONE);
            tvForm.setVisibility(View.GONE);
            PreferenceUtil.setBase64Image(this, "");
        }else{
            Glide.with(this).load(transaction.getFilePathForm()).into(imvPhoto);
            viewForm.setVisibility(View.VISIBLE);
            tvForm.setVisibility(View.VISIBLE);
        }
        Glide.with(this).load(transaction.getFilePathInvoice()).into(imvInvoice);

        // For
        String mFor = patientName + "\n" + contact;
        tvFor.setText(mFor);
        if (mode == Constant.FROM_CALENDAR) {
            // At
            tvAt.setText(event.getName());
            String mAddress = event.getmAddress() + "\n" + "Tel: " + event.getmPhone();
            tvAddress.setText(mAddress);

            // On
            String mDayOfWeek = Utils.DayOfWeek(this, calendarHospital.getmDayOfWeek());
            String mMonthOfYear = Utils.MonthOfYear(this, calendarHospital.getmMonthOfYear());
            String apm = "AM";
            if (calendarHospital.getmHour() > 12) {
                apm = "PM";
            }
            String date = mDayOfWeek + ",\t" + calendarHospital.getmDayOfMonth() + "\t" + mMonthOfYear + "\t" +
                    calendarHospital.getmYear();
            tvOn.setText(date);
            String time = calendarHospital.getmHour() + ":" + calendarHospital.getmMinutes() + apm;
            tvHour.setText(time);
            // map
            String location = event.getLocation();
            viewMap(Double.parseDouble(location.split(",")[0]), Double.parseDouble(location.split(",")[1]));
        } else {
            // On
            tvOn.setText(Utils.timeFromHistory(this, transaction.getBooking_time()));
            String hour = transaction.getBooking_time().substring(8, 10);
            String apm = "AM";
            if (Integer.parseInt(hour) > 12) {
                apm = "PM";
            }
            String time = hour + ":" + "00" + apm;
            tvHour.setText(time);
            // At
            tvAt.setText(clinic.getName());
            String mAddress = clinic.getAddress() + "\n" + "Tel: " + clinic.getPhone();
            tvAddress.setText(mAddress);
            // map
            viewMap(clinic.getLatitude(), clinic.getLongitude());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                Intent intent = new Intent(this, SelectedSlotActivity.class);
                intent.putExtra(Constant.TRANSACTION, transaction);
                intent.putExtra(Constant.DATE, tvOn.getText().toString());
                intent.putExtra(Constant.TIME, tvHour.getText().toString());
                intent.putExtra(Constant.CHANGE_FORM, isChangeForm);
                intent.putExtra(Constant.CHANGE_INVOICE, isChangeInvoice);
                if (mode == Constant.FROM_CALENDAR) {
                    intent.putExtra(Constant.CLINIC, event);
                    intent.putExtra(Constant.MODE, Constant.FROM_CALENDAR);
                } else {
                    intent.putExtra(Constant.CLINIC, clinic);
                    intent.putExtra(Constant.MODE, Constant.FROM_HISTORY);
                }
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btnPrint:
                break;
        }
    }

    private void viewMap(final double latitude, final double longitude) {
        mMap.onResume();
        // show map
        try {
            MapsInitializer.initialize(this.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMap.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mGoogleMap = mMap;
                LatLng sydney = new LatLng(latitude, longitude);
                // googleMap.setOnMyLocationChangeListener(myLocationChangeListener);
                mGoogleMap.addMarker(new MarkerOptions().position(sydney));
                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(13).build();
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mMap.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMap.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMap.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMap.onLowMemory();
    }
}
