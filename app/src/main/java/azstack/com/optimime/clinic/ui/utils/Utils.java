package azstack.com.optimime.clinic.ui.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.io.ByteArrayOutputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

import azstack.com.optimime.clinic.R;

/**
 * Created by Dang Luu on 2/16/2017.
 */

public class Utils {
    public static boolean checkNetwork(Context context) {
        NetworkInfo info = (NetworkInfo) ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo();

        if (info == null || !info.isConnected()) {
            return false;
        }
        if (info.isRoaming()) {
            return false;
        }
        return true;
    }

    public static void galleryIntentGallery(FragmentActivity activity, int requestKey) {
        Intent photoIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoIntent.setType("image/*");
        activity.startActivityForResult(photoIntent, requestKey);
    }

    public static void galleryIntentGalleryFragment(Fragment fragment, int requestKey) {
        Intent photoIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoIntent.setType("image/*");
        fragment.startActivityForResult(photoIntent, requestKey);
    }

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    public static boolean checkPermissionCamera(Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            }
        }
        return true;
    }

    public static boolean checkPermission(int[] resultAccept) {
        for (int i : resultAccept) {
            if (i != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkPermissionReadExternalStorage(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static boolean hasLatLon(Context context) {
        double lat = PreferenceUtil.getLat(context);
        double lon = PreferenceUtil.getLon(context);

        if (lat == 0 || lon == 0) {
            return false;
        }
        return true;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void hideSoftKeyboardWithEditText(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showSoftKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void forceShowKeyboard(Context context) {
        ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(
                InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

//    public void changeDate(long beginDate){
//        CalendarHospital begin = CalendarHospital.getInstance();
//        begin.setTimeInMillis(beginDate);
//
//        CalendarHospital current = CalendarHospital.getInstance();
//
//        int years =  current.get(CalendarHospital.YEAR) - begin.get(CalendarHospital.YEAR);
//        int months =  current.get(CalendarHospital.YEAR) - begin.get(CalendarHospital.YEAR);
//        int years =  current.get(CalendarHospital.YEAR) - begin.get(CalendarHospital.YEAR);
//
//    }

    public static String DayOfWeek(Context context, int day) {
        String mDayOfWeek = "";
        if (day == 1) {
            mDayOfWeek = context.getString(R.string.lbl_sun);
        } else if (day == 2) {
            mDayOfWeek = context.getString(R.string.lbl_mon);
        } else if (day == 3) {
            mDayOfWeek = context.getString(R.string.lbl_tue);
        } else if (day == 4) {
            mDayOfWeek = context.getString(R.string.lbl_wed);
        } else if (day == 5) {
            mDayOfWeek = context.getString(R.string.lbl_thur);
        } else if (day == 6) {
            mDayOfWeek = context.getString(R.string.lbl_fri);
        } else if (day == 7) {
            mDayOfWeek = context.getString(R.string.lbl_sat);
        }
        return mDayOfWeek;
    }

    public static String MonthOfYear(Context context, int month) {
        String mMonthOfYear = "";
        if (month == 1) {
            mMonthOfYear = context.getString(R.string.lbl_jan);
        } else if (month == 2) {
            mMonthOfYear = context.getString(R.string.lbl_feb);
        } else if (month == 3) {
            mMonthOfYear = context.getString(R.string.lbl_march);
        } else if (month == 4) {
            mMonthOfYear = context.getString(R.string.lbl_april);
        } else if (month == 5) {
            mMonthOfYear = context.getString(R.string.lbl_may);
        } else if (month == 6) {
            mMonthOfYear = context.getString(R.string.lbl_june);
        } else if (month == 7) {
            mMonthOfYear = context.getString(R.string.lbl_july);
        } else if (month == 8) {
            mMonthOfYear = context.getString(R.string.lbl_august);
        } else if (month == 9) {
            mMonthOfYear = context.getString(R.string.lbl_sep);
        } else if (month == 10) {
            mMonthOfYear = context.getString(R.string.lbl_oct);
        } else if (month == 11) {
            mMonthOfYear = context.getString(R.string.lbl_nov);
        } else if (month == 12) {
            mMonthOfYear = context.getString(R.string.lbl_dec);
        }
        return mMonthOfYear;
    }

    public static String DateTime(Context context, String time) {
        String dateTime = "";
        String dayOfMonth = time.substring(6, 8);
        String month = time.substring(4, 6);
        String year = time.substring(1, 4);
        Calendar myCalendar = new GregorianCalendar(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(dayOfMonth));
        int dayOfWeek = myCalendar.get(Calendar.DAY_OF_WEEK);
        String day = Utils.DayOfWeek(context, dayOfWeek);
        dateTime = day + " " + dayOfMonth + " " + Utils.MonthOfYear(context, Integer.parseInt(month));
        return dateTime;
    }

    public static String timeFromHistory(Context context, String time) {
        String dateTime = "";
        String dayOfMonth = time.substring(6, 8);
        String month = time.substring(4, 6);
        String year = time.substring(1, 4);
        Calendar myCalendar = new GregorianCalendar(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(dayOfMonth));
        int dayOfWeek = myCalendar.get(Calendar.DAY_OF_WEEK);
        String day = Utils.DayOfWeek(context, dayOfWeek);
        dateTime = day + ", " + dayOfMonth + " " + Utils.MonthOfYear(context, Integer.parseInt(month)) + " " + year;
        return dateTime;
    }

    public static String TimeKey(Calendar time) {
        int start = time.get(Calendar.HOUR_OF_DAY);
        String year = String.valueOf(time.get(Calendar.YEAR));
        String month = String.valueOf(time.get(Calendar.MONTH) + 1);
        String startTimeKey;
        if (start < 10) {
            startTimeKey = "0" + start;
        } else {
            startTimeKey = start + "";
        }
        String dayOfMonth = String.valueOf(time.get(Calendar.DAY_OF_MONTH));
        if (time.get(Calendar.DAY_OF_MONTH) < 10) {
            dayOfMonth = "0" + time.get(Calendar.DAY_OF_MONTH);
        }
        if ((time.get(Calendar.MONTH) + 1) < 10) {
            month = "0" + (time.get(Calendar.MONTH) + 1);
        }
        String timeKey = year + month + dayOfMonth + startTimeKey;

        return timeKey;
    }

    public static long convertStringToTimestamp(String str_date) {
        try {
            DateFormat formatter;
            formatter = new SimpleDateFormat("yyyyMMddHH");
            // you can change format of date
            Date date = formatter.parse(str_date);
            Timestamp timeStampDate = new Timestamp(date.getTime());
            long timestamp = timeStampDate.getTime() / 10000;
            return timestamp;
        } catch (ParseException e) {
            System.out.println("Exception :" + e);
            return 0;
        }
    }

    private static final Pattern NRIC_PATTERN = Pattern.compile("[A-Za-z]");
    private static final Pattern NUMBER_PATTERN = Pattern.compile("([0-9])");

    public static boolean isValidNRIC(String text) {
        return NRIC_PATTERN.matcher(text).matches();
    }

    public static boolean isValidNumber(String str) {
        boolean notIsNumber = false;
        for (int i = 0; i < str.length(); i++) {
            // Determines if the specified character is a digit
            if (!Character.isDigit(str.charAt(i))) {
                notIsNumber = true;
                break;
            }
        }
        return notIsNumber;
    }
}
