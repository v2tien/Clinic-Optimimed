package azstack.com.optimime.clinic.ui.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.adapter.ListClinicAdapter;
import azstack.com.optimime.clinic.ui.model.CalendarClinic;
import azstack.com.optimime.clinic.ui.model.Result;
import azstack.com.optimime.clinic.ui.model.Transaction;
import azstack.com.optimime.clinic.ui.service.Notify;
import azstack.com.optimime.clinic.ui.utils.Constant;
import azstack.com.optimime.clinic.ui.utils.HospitalRequestUtils;
import azstack.com.optimime.clinic.ui.utils.PreferenceUtil;
import azstack.com.optimime.clinic.ui.utils.Utils;
import azstack.com.optimime.clinic.ui.utils.VolleyUtils;

/**
 * Created by VuVan on 25/03/2017.
 */

public class MainClinicActivity extends BaseActivityHasBackButton implements WeekView.EventClickListener,
        MonthLoader.MonthChangeListener, WeekView.EventLongPressListener, WeekView.EmptyViewLongPressListener,
        WeekView.EmptyViewClickListener {

    private RecyclerView mRecycleView, mRecycleSearch;
    private LinearLayout viewCapacity, viewAppointments, viewBottom;
    private ListClinicAdapter clinicAdapter, clinicSearchAdapter;
    private LinearLayoutManager layoutManager;
    private WeekView mWeekView;
    private TextView tvResult;

    private List<WeekViewEvent> events = new ArrayList<>();
    private List<Transaction> transactions = new ArrayList<>();
    private List<Transaction> searchTransactions = new ArrayList<>();

    private long mClinicId;
    private boolean isLoad = false;

    private MenuItem menuSearch, menuToday;
    private SearchView searchView;

    private BroadcastReceiver dataPush;
    private BroadcastReceiver updateStatus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_clinic);

        mClinicId = PreferenceUtil.getMyId(this);

        initComponent();
        initListener();
        registerReceiver();
    }

    private void initComponent() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle(PreferenceUtil.getName(this));
        getSupportActionBar().setSubtitle("Consultation Timings");
        mWeekView = (WeekView) findViewById(R.id.weekView);
        viewBottom = (LinearLayout) findViewById(R.id.view_bottom);
        viewCapacity = (LinearLayout) findViewById(R.id.viewCapacity);
        viewCapacity.setOnClickListener(this);
        viewAppointments = (LinearLayout) findViewById(R.id.viewAppointments);
        viewAppointments.setOnClickListener(this);
        mRecycleSearch = (RecyclerView) findViewById(R.id.listSearch);
        mRecycleView = (RecyclerView) findViewById(R.id.listClinic);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecycleView.setLayoutManager(layoutManager);
        tvResult = (TextView) findViewById(R.id.tvResult);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    private void initListener() {
        // Show a toast message about the touched event.
        mWeekView.setOnEventClickListener(this);
        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        mWeekView.setMonthChangeListener(this);
        // Set long press listener for events.
        mWeekView.setEventLongPressListener(this);
        // Set long press listener for empty view
        mWeekView.setEmptyViewLongPressListener(this);

        mWeekView.setEmptyViewClickListener(this);
        // Set up a date time interpreter to interpret how the date and time will be formatted in
        // the week view. This is optional.
        setupDateTimeInterpreter(false);

        getClinicCalendar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        menuSearch = menu.findItem(R.id.search);
        menuToday = menu.findItem(R.id.action_today);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        MenuItemCompat.setOnActionExpandListener(menu.findItem(R.id.search), new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                //The SearchView is opening
                viewBottom.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                //The SearchView is closing. Do stuff here.
                viewBottom.setVisibility(View.VISIBLE);
                searchTransactions.clear();
                mRecycleSearch.setVisibility(View.GONE);
                if (transactions.isEmpty()) {
                    mRecycleView.setVisibility(View.GONE);
                    tvResult.setVisibility(View.VISIBLE);
                    tvResult.setText(R.string.no_history);
                } else {
                    mRecycleView.setVisibility(View.VISIBLE);
                    tvResult.setVisibility(View.GONE);
                }
                return true;
            }
        });
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // handles a search query
            searchView.setFocusable(false);
            this.getCurrentFocus().clearFocus();
            String query = intent.getStringExtra(SearchManager.QUERY);
            getListSearchTransaction(query);
        }
    }

//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        menuSearch.setVisible(false);
//        menuToday.setVisible(true);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_today:
                mWeekView.goToToday();
                break;
            case R.id.action_logout:
                showLogout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.viewCapacity:
                if (isLoad == false) {
                    transactions.clear();
                    getListTransaction();
                }
                mRecycleView.setVisibility(view.VISIBLE);
                mWeekView.setVisibility(view.GONE);
                viewCapacity.setBackgroundResource(R.drawable.bg_btn_clinic_blue);
                viewAppointments.setBackgroundResource(R.drawable.bg_btn_clinic_gray);

                if (transactions.isEmpty()) {
                    tvResult.setVisibility(view.VISIBLE);
                }

                menuToday.setVisible(false);
                menuSearch.setVisible(true);
                break;
            case R.id.viewAppointments:
                mRecycleView.setVisibility(view.GONE);
                mWeekView.setVisibility(view.VISIBLE);
                viewCapacity.setBackgroundResource(R.drawable.bg_btn_clinic_gray);
                viewAppointments.setBackgroundResource(R.drawable.bg_btn_clinic_blue);
                tvResult.setVisibility(view.GONE);

                menuToday.setVisible(true);
                menuSearch.setVisible(false);
                break;
        }
    }

    /**
     * Set up a date time interpreter which will show short date values when in week view and long
     * date values otherwise.
     *
     * @param shortDate True if the date values should be short.
     */
    private void setupDateTimeInterpreter(final boolean shortDate) {
        mWeekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {
                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", Locale.getDefault());
                String weekday = weekdayNameFormat.format(date.getTime());
                SimpleDateFormat format = new SimpleDateFormat(" M/d", Locale.getDefault());
                // All android api level do not have a standard way of getting the first letter of
                // the week day name. Hence we get the first char programmatically.
                // Details: http://stackoverflow.com/questions/16959502/get-one-letter-abbreviation-of-week-day-of-a-date-in-java#answer-16959657
                if (shortDate)
                    weekday = String.valueOf(weekday.charAt(0));
                return weekday.toUpperCase() + format.format(date.getTime());
            }

            @Override
            public String interpretTime(int hour) {
                String time;
                if (hour == 0) {
                    time = "0 AM";
                } else if (hour == 12) {
                    time = "12 PM";
                } else if (hour > 11) {
                    time = (hour - 12) + " PM";
                } else {
                    time = hour + " AM";
                }
                return time;
            }
        });
    }

    /**
     * CalendarHospital
     */
    @Override
    public void onEmptyViewLongPress(Calendar time) {
    }

    @Override
    public void onEmptyViewClicked(final Calendar time) {
        Calendar now = Calendar.getInstance();
        final String timeKey = Utils.TimeKey(time);
        String timeNow = Utils.TimeKey(now);
//        Log.e("time", timeKey + "--" + timeNow);
        long selectLong = Utils.convertStringToTimestamp(timeKey);
        long nowLong = Utils.convertStringToTimestamp(timeNow);
        if (selectLong < nowLong) {
            Toast.makeText(MainClinicActivity.this, "Invalid", Toast.LENGTH_SHORT).show();
            return;
        }
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_change_event);

        TextView btnOk = (TextView) dialog.findViewById(R.id.btn_positive);
        TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_negative);
        final EditText edtSot = (EditText) dialog.findViewById(R.id.edtEvent);
        final AppCompatCheckBox radioClose = (AppCompatCheckBox) dialog.findViewById(R.id.radioClosed);
        final AppCompatCheckBox radioOpen = (AppCompatCheckBox) dialog.findViewById(R.id.radioOpen);
        final AppCompatCheckBox radioOther = (AppCompatCheckBox) dialog.findViewById(R.id.radioOther);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Integer> arrType = new ArrayList<>();
                if (radioClose.isChecked())
                    arrType.add(1);
                if (radioOpen.isChecked())
                    arrType.add(2);
                if (radioOther.isChecked())
                    arrType.add(3);

                if (!edtSot.getText().toString().isEmpty() && !arrType.isEmpty()) {
                    createNewSlot(edtSot.getText().toString(), timeKey, arrType);
                    dialog.dismiss();
                } else {
                    Toast.makeText(MainClinicActivity.this, "Please enter slot or choose type hospital!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onEventClick(final WeekViewEvent event, RectF eventRect, Calendar time) {
        Calendar now = Calendar.getInstance();
        final String timeKey = Utils.TimeKey(time);
        String timeNow = Utils.TimeKey(now);
        long selectLong = Utils.convertStringToTimestamp(timeKey);
        long nowLong = Utils.convertStringToTimestamp(timeNow);
        if (selectLong < nowLong) {
            Toast.makeText(MainClinicActivity.this, "Invalid", Toast.LENGTH_SHORT).show();
            return;
        }
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_change_event);
        final ArrayList<Integer> arrType = new ArrayList<>();

        TextView btnOk = (TextView) dialog.findViewById(R.id.btn_positive);
        TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_negative);
        final EditText edtSot = (EditText) dialog.findViewById(R.id.edtEvent);
        final AppCompatCheckBox radioClose = (AppCompatCheckBox) dialog.findViewById(R.id.radioClosed);
        final AppCompatCheckBox radioOpen = (AppCompatCheckBox) dialog.findViewById(R.id.radioOpen);
        final AppCompatCheckBox radioOther = (AppCompatCheckBox) dialog.findViewById(R.id.radioOther);
//        RadioGroup radioGroup = (RadioGroup)dialog.findViewById(R.id.radioGroup);
        edtSot.setText(event.getName());
        edtSot.setSelection(event.getName().length());
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        // hien thi neu da chon

        if (event.getType() != null) {
            String type = event.getType().replace(" ","");
            String subType = type.substring(1, (type.length() - 1));
            Log.e("type=", subType + "");
            String[] arr = subType.split(",");
            Log.e("arr", arr.length+"");
            for (int i = 0; i< arr.length; i++){
                if(arr[i].equals("1")){
                    radioClose.setChecked(true);
                }else if(arr[i].equals("2")){
                    radioOpen.setChecked(true);
                }else if(arr[i].equals("3")){
                    radioOther.setChecked(true);
                }
            }
        }

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioClose.isChecked())
                    arrType.add(1);
                if (radioOpen.isChecked())
                    arrType.add(2);
                if (radioOther.isChecked())
                    arrType.add(3);

                if (!edtSot.getText().toString().isEmpty() && !arrType.isEmpty()) {
                    updateSlot(event, edtSot.getText().toString(), timeKey, arrType);
                } else {
                    updateSlot(event, "0", timeKey, arrType);
                }
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
        Toast.makeText(this, "Long pressed event= " + event.getSlot(), Toast.LENGTH_SHORT).show();
    }

    public WeekView getWeekView() {
        return mWeekView;
    }

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        // Return only the events that matches newYear and newMonth.
        List<WeekViewEvent> matchedEvents = new ArrayList<WeekViewEvent>();
        for (WeekViewEvent event : events) {
            if (eventMatches(event, newYear, newMonth)) {
                matchedEvents.add(event);
            }
        }
        return matchedEvents;
    }

    /**
     * Checks if an event falls into a specific year and month.
     *
     * @param event The event to check for.
     * @param year  The year.
     * @param month The month.
     * @return True if the event matches the year and month.
     */
    private boolean eventMatches(WeekViewEvent event, int year, int month) {
        return (event.getStartTime().get(Calendar.YEAR) == year && (event.getStartTime().get(Calendar.MONTH)) == month - 1)
                || (event.getEndTime().get(Calendar.YEAR) == year && (event.getEndTime().get(Calendar.MONTH)) == month - 1);
    }

    private void getClinicCalendar() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();

        HospitalRequestUtils.getClinicCalendar(this, PreferenceUtil.getToken(getBaseContext()), mClinicId, new VolleyUtils.OnRequestListenner() {
            @Override
            public void onSussces(String response, Result result) {
                progressDialog.dismiss();
                if (result.isSuccess()) {
                    events.clear();
                    List<CalendarClinic> listCalendar = VolleyUtils.getListModelFromRespone(response, CalendarClinic.class);
                    for (CalendarClinic event : listCalendar) {
                        events.add(event.toWeekViewEvent());
                    }
                    getWeekView().notifyDatasetChanged();
                } else {
                    Toast.makeText(MainClinicActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
            }
        });
    }

    private void createNewSlot(final String slot, final String timeKey, final ArrayList<Integer> type) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        HospitalRequestUtils.requestCreateSlot(this, PreferenceUtil.getToken(getBaseContext()), mClinicId,
                timeKey, slot, type, new VolleyUtils.OnRequestListenner() {
                    @Override
                    public void onSussces(String response, Result result) {
                        progressDialog.dismiss();
                        if (result.isSuccess()) {
                            CalendarClinic event = (CalendarClinic) VolleyUtils.getObject(response, CalendarClinic.class);
                            events.add(event.toWeekViewEvent());
                            getWeekView().notifyDatasetChanged();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        progressDialog.dismiss();
                    }
                });
    }

    private void updateSlot(final WeekViewEvent event, final String slot, final String timeKey, final ArrayList<Integer> type) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        Log.e("token", PreferenceUtil.getToken(getBaseContext()));
        HospitalRequestUtils.requestUpdateSlot(this, PreferenceUtil.getToken(getBaseContext()),
                event.getId(), mClinicId, timeKey, slot, type, new VolleyUtils.OnRequestListenner() {
                    @Override
                    public void onSussces(String response, Result result) {
                        progressDialog.dismiss();
                        if (result.isSuccess()) {
                            for (int i = 0; i < events.size(); i++) {
                                if (events.get(i).getId() == event.getId()) {
                                    if (!slot.isEmpty()) {
                                        // edit event
                                        events.get(i).setType(type.toString());
                                        events.get(i).setName(slot);
                                    } else {
                                        // remove event
//                                        events.remove(i);
                                    }
                                    getWeekView().notifyDatasetChanged();
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(String error) {
                        progressDialog.dismiss();
                    }
                });
    }

    private void getListTransaction() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();

        HospitalRequestUtils.getTransaction(this, PreferenceUtil.getToken(getBaseContext()), mClinicId, new VolleyUtils.OnRequestListenner() {
            @Override
            public void onSussces(String response, Result result) {
                progressDialog.dismiss();
                if (result.isSuccess()) {
                    isLoad = true;
                    List<Transaction> listTransaction = VolleyUtils.getListTransactionResponse(response);
                    transactions.addAll(listTransaction);
                    if (!transactions.isEmpty()) {
                        tvResult.setVisibility(View.GONE);
                        mRecycleView.setVisibility(View.VISIBLE);
                        if (clinicAdapter == null) {
                            clinicAdapter = new ListClinicAdapter(MainClinicActivity.this, transactions, 0);
//                            mRecycleView.setLayoutManager(new LinearLayoutManager(MainClinicActivity.this));
                            mRecycleView.setAdapter(clinicAdapter);
                        } else {
                            clinicAdapter.setList(transactions);
                        }
                    } else {
                        tvResult.setVisibility(View.VISIBLE);
                        mRecycleView.setVisibility(View.GONE);
                    }
                } else {
                    Toast.makeText(MainClinicActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
            }
        });
    }

    private void getListSearchTransaction(String search) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        HospitalRequestUtils.getListSearchTransaction(this, PreferenceUtil.getToken(this), search,
                new VolleyUtils.OnRequestListenner() {
                    @Override
                    public void onSussces(String response, Result result) {
                        progressDialog.dismiss();
                        if (result.isSuccess()) {
                            searchTransactions = VolleyUtils.getListModelFromRespone(response, Transaction.class);
                            if (!searchTransactions.isEmpty()) {
                                tvResult.setVisibility(View.GONE);
                                mRecycleSearch.setVisibility(View.GONE);
                                mRecycleSearch.setVisibility(View.VISIBLE);
                                clinicSearchAdapter = new ListClinicAdapter(MainClinicActivity.this, searchTransactions, 0);
                                mRecycleSearch.setLayoutManager(new LinearLayoutManager(MainClinicActivity.this));
                                mRecycleSearch.setAdapter(clinicSearchAdapter);
                            } else {
                                tvResult.setVisibility(View.VISIBLE);
                                tvResult.setText(R.string.no_history);
                                mRecycleView.setVisibility(View.GONE);
                                mRecycleSearch.setVisibility(View.GONE);
                            }
                        } else {
                            mRecycleView.setVisibility(View.GONE);
                            mRecycleSearch.setVisibility(View.GONE);
                            tvResult.setVisibility(View.VISIBLE);
                            tvResult.setText(result.getMessage());
                        }
                    }

                    @Override
                    public void onError(String error) {
                        progressDialog.dismiss();
                        Log.e("onError", error);
                    }
                });
    }

    private void showLogout() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);

        TextView btnOk = (TextView) dialog.findViewById(R.id.btn_positive);
        TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_negative);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceUtil.setToken(MainClinicActivity.this, "");
                PreferenceUtil.clearData(MainClinicActivity.this);
                startActivity(new Intent(MainClinicActivity.this, ChooseActivity.class));
                finish();
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void registerReceiver() {
        IntentFilter filter = new IntentFilter(Notify.DATA_PUSH.getValue());
        dataPush = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent data) {
                Transaction newTrans = data.getParcelableExtra(Constant.TRANSACTION);
                if (!transactions.isEmpty()) {
                    String date = transactions.get(transactions.size()).getBooking_time().substring(1, 8);
                    String dateNew = newTrans.getBooking_time().substring(1, 8);
                    if (dateNew.equals(date)) {
                        newTrans.setHideTime(1);
                    } else {
                        newTrans.setHideTime(0);
                    }
                }
                transactions.add(transactions.size(), newTrans);
                if (clinicAdapter == null) {
                    clinicAdapter = new ListClinicAdapter(MainClinicActivity.this, transactions, 0);
                    mRecycleView.setAdapter(clinicAdapter);
                } else {
                    clinicAdapter.setList(transactions);
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(dataPush, filter);

        IntentFilter filter1 = new IntentFilter(Notify.UPDATE_STATUS.getValue());
        updateStatus = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent data) {
                int pos = data.getIntExtra(Constant.POSITION, 0);
                int status = data.getIntExtra(Constant.STATUS, 1);
                long transactionId = data.getLongExtra(Constant.ID, 0);
                for (int i = 0; i < transactions.size(); i++) {
                    if (transactions.get(i).getId() == transactionId) {
                        transactions.get(i).setStatus(status);
                        clinicAdapter.setList(transactions);
                    }
                }
                if (!searchTransactions.isEmpty()) {
                    searchTransactions.get(pos).setStatus(status);
                    clinicSearchAdapter.setList(searchTransactions);
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(updateStatus, filter1);

    }

    public void unregisterReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(dataPush);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(updateStatus);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver();
        Log.e("onDestroy", "onDestroy");
        super.onDestroy();
    }
}
