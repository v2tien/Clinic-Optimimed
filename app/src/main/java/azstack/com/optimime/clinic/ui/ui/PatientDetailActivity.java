package azstack.com.optimime.clinic.ui.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.model.Patient;
import azstack.com.optimime.clinic.ui.model.Result;
import azstack.com.optimime.clinic.ui.model.Transaction;
import azstack.com.optimime.clinic.ui.service.NotifyService;
import azstack.com.optimime.clinic.ui.utils.Constant;
import azstack.com.optimime.clinic.ui.utils.HospitalRequestUtils;
import azstack.com.optimime.clinic.ui.utils.PreferenceUtil;
import azstack.com.optimime.clinic.ui.utils.Utils;
import azstack.com.optimime.clinic.ui.utils.VolleyUtils;

/**
 * Created by VuVan on 25/03/2017.
 */

public class PatientDetailActivity extends BaseActivityHasBackButton {

    private int position;
    private TextView tvName, tvTime, tvRef, tvPhone, tvDepartment, tvConsultant, tvICCard;
    private ImageView imvPhoto;
    private LinearLayout btnPatientArrives, viewArrive, viewSuccessfully;
    private Button btnNoShow, btnSuccessfully, btnUnSuccessfully, btnArrived;
    private Transaction transaction;
    private int type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_detail);

//        patientId = getIntent().getLongExtra(Constant.PATIENT_ID, 0);
//        transactionId = getIntent().getLongExtra(Constant.ID, 0);
        position = getIntent().getIntExtra(Constant.POSITION, 0);
        type = getIntent().getIntExtra(Constant.CHOOSE_TYPE, 0);
        transaction = getIntent().getParcelableExtra(Constant.TRANSACTION);
//        status = getIntent().getIntExtra(Constant.STATUS, 0);
//        bookingTime = getIntent().getStringExtra(Constant.TIME_KEY);

        initComponent();

//        getDetail();
    }

    private void initComponent() {
        getSupportActionBar().setTitle(R.string.lbl_patient_detail);
        tvName = (TextView) findViewById(R.id.tvName);
        tvTime = (TextView) findViewById(R.id.tv_time);
        tvRef = (TextView) findViewById(R.id.tv_ref_no);
        tvPhone = (TextView) findViewById(R.id.tv_tel);
        tvDepartment = (TextView) findViewById(R.id.tvDepartment);
        tvConsultant = (TextView) findViewById(R.id.tvConsultant);
        tvICCard = (TextView) findViewById(R.id.tvICCard);
        imvPhoto = (ImageView) findViewById(R.id.imvPhoto);
//        btnPatientArrives = (LinearLayout) findViewById(R.id.viewConfirm);
//        btnPatientArrives.setOnClickListener(this);
        btnSuccessfully = (Button) findViewById(R.id.btnSuccessfully);
        btnSuccessfully.setOnClickListener(this);
        btnUnSuccessfully = (Button) findViewById(R.id.btnUnSuccessfully);
        btnUnSuccessfully.setOnClickListener(this);
        btnNoShow = (Button) findViewById(R.id.btnNoShow);
        btnNoShow.setOnClickListener(this);
        btnArrived = (Button) findViewById(R.id.btnArrived);
        btnArrived.setOnClickListener(this);
        viewArrive = (LinearLayout) findViewById(R.id.view_arrive);
        viewSuccessfully = (LinearLayout) findViewById(R.id.view_success);

        fillData();
    }

    private void getDetail() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        HospitalRequestUtils.getPatientDetail(this, PreferenceUtil.getToken(getBaseContext()),
                transaction.getPatient_id(), new VolleyUtils.OnRequestListenner() {
                    @Override
                    public void onSussces(String response, Result result) {
                        progressDialog.dismiss();
                        if (result.isSuccess()) {
                            Patient patient = (Patient) VolleyUtils.getObject(response, Patient.class);
                            tvName.setText(patient.getFullname());
                            tvPhone.setText(patient.getPhone());
                            Glide.with(PatientDetailActivity.this)
                                    .load(patient.getScanned_profile())
                                    .dontAnimate().into(imvPhoto);
//                            tvRef.setText("000"+transactionId+"");
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(PatientDetailActivity.this, "Error", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        progressDialog.dismiss();
                    }
                });
    }

    private void fillData() {
        tvName.setText(transaction.getPatient().getFullname());
        tvPhone.setText(transaction.getPatient().getPhone());
        Glide.with(this).load(transaction.getPatient().getScanned_profile())
                .dontAnimate().into(imvPhoto);
        tvRef.setText("000" + transaction.getId() + "");
        if (transaction.getHospital_department().isEmpty()) {
            tvDepartment.setText(getString(R.string.unknown));
        } else {
            tvDepartment.setText(transaction.getHospital_department());
        }
        if (transaction.getConsultant_name().isEmpty()) {
            tvConsultant.setText(getString(R.string.unknown));
        } else {
            tvConsultant.setText(transaction.getConsultant_name());
        }
        if (transaction.getPatient().getIdentify_card().isEmpty()) {
            tvICCard.setText(getString(R.string.unknown));
        } else {
            tvICCard.setText(transaction.getPatient().getIdentify_card());
        }

        String time = transaction.getBooking_time().substring(8, 10);
        if (Integer.parseInt(time) > 12) {
            time = time + ":00 PM";
        } else {
            time = time + ":00 AM";
        }
        tvTime.setText(time);
        if (type == 0) {
            if (transaction.getStatus() == 1) {
                viewArrive.setVisibility(View.VISIBLE);
                viewSuccessfully.setVisibility(View.GONE);
                btnArrived.setEnabled(true);
                btnArrived.setBackgroundResource(R.drawable.bg_btn_clinic);
                btnNoShow.setEnabled(true);
                btnNoShow.setBackgroundResource(R.drawable.bg_btn_clinic);
            } else if (transaction.getStatus() == 5) {
                viewArrive.setVisibility(View.VISIBLE);
                viewSuccessfully.setVisibility(View.GONE);
                btnArrived.setEnabled(true);
                btnArrived.setBackgroundResource(R.drawable.bg_btn_clinic);
                btnNoShow.setEnabled(false);
                btnNoShow.setBackgroundResource(R.drawable.bg_btn_clinic_disable);
            } else if (transaction.getStatus() == 6 || transaction.getStatus() == 7) {
                viewArrive.setVisibility(View.GONE);
                viewSuccessfully.setVisibility(View.GONE);
            } else {
                viewArrive.setVisibility(View.GONE);
                viewSuccessfully.setVisibility(View.VISIBLE);
            }
        }else{
            viewSuccessfully.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btnArrived:
                Log.e("token", PreferenceUtil.getToken(getBaseContext()));
                updateStatus(3, "");
                break;
            case R.id.btnNoShow:
                updateStatus(5, "");
                break;
            case R.id.btnSuccessfully:
                showRemark(6);
                break;
            case R.id.btnUnSuccessfully:
                showRemark(7);
                break;
        }
    }

    private void updateStatus(final int status, String note) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        HospitalRequestUtils.updateTransaction(this, PreferenceUtil.getToken(getBaseContext()), transaction.getId(),
                status, note, new VolleyUtils.OnRequestListenner() {
                    @Override
                    public void onSussces(String response, Result result) {
                        progressDialog.dismiss();
                        if (result.isSuccess()) {
                            Toast.makeText(PatientDetailActivity.this, R.string.successfully, Toast.LENGTH_SHORT).show();
                            NotifyService.notifyUpdateStatus(PatientDetailActivity.this, status, position, transaction.getId());
                            if (status == 3) {
                                btnArrived.setEnabled(false);
                                btnArrived.setBackgroundResource(R.drawable.bg_btn_clinic_disable);
                                btnNoShow.setEnabled(false);
                                btnNoShow.setBackgroundResource(R.drawable.bg_btn_clinic_disable);
                                viewSuccessfully.setVisibility(View.VISIBLE);
                            } else if (status == 5) {
                                btnArrived.setEnabled(false);
                                btnArrived.setBackgroundResource(R.drawable.bg_btn_clinic_disable);
                                btnNoShow.setEnabled(false);
                                btnNoShow.setBackgroundResource(R.drawable.bg_btn_clinic_disable);
                            } else if (status == 6 || status == 7) {
//                                btnSuccessfully.setEnabled(false);
//                                btnSuccessfully.setBackgroundResource(R.drawable.bg_btn_clinic_disable);
//                                btnUnSuccessfully.setEnabled(false);
//                                btnUnSuccessfully.setBackgroundResource(R.drawable.bg_btn_clinic_disable);
                                btnArrived.setVisibility(View.GONE);
                                btnNoShow.setVisibility(View.GONE);
                                btnSuccessfully.setVisibility(View.GONE);
                                btnUnSuccessfully.setVisibility(View.GONE);
                            }
                        } else {
                            Toast.makeText(PatientDetailActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        progressDialog.dismiss();
                    }
                });
    }

    private void showRemark(final int status) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_remark);

        TextView btnSave = (TextView) dialog.findViewById(R.id.btn_positive);
        TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_negative);
        final EditText edtRemark = (EditText) dialog.findViewById(R.id.edtRemark);
        Utils.forceShowKeyboard(PatientDetailActivity.this);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateStatus(status, edtRemark.getText().toString());
                dialog.dismiss();
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateStatus(status, "");
                dialog.dismiss();
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}
