package azstack.com.optimime.clinic.ui.service;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import azstack.com.optimime.clinic.ui.model.Transaction;
import azstack.com.optimime.clinic.ui.utils.Constant;


public class NotifyService {

    public static void notifyPush(Context context, Transaction transaction) {
        Intent intent = new Intent(Notify.DATA_PUSH.getValue());
        intent.putExtra(Constant.TRANSACTION, transaction);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void notifyUpdateStatus(Context context, int status, int pos, long transactionId) {
        Intent intent = new Intent(Notify.UPDATE_STATUS.getValue());
        intent.putExtra(Constant.STATUS, status);
        intent.putExtra(Constant.POSITION, pos);
        intent.putExtra(Constant.ID, transactionId);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
