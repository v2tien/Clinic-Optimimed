package azstack.com.optimime.clinic.ui.ui;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.utils.Constant;
import azstack.com.optimime.clinic.ui.utils.PreferenceUtil;

/**
 * Created by VuVan on 23/03/2017.
 */

public class SelectBookActivity extends BaseActivity {

    private Button btnClosed, btnOpen, btnOther;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_book);

        initComponent();
    }

    private void initComponent(){
        getSupportActionBar().setTitle(PreferenceUtil.getName(this));

        btnClosed = (Button)findViewById(R.id.btnClosed);
        btnOpen = (Button)findViewById(R.id.btnOpen);
        btnOther = (Button)findViewById(R.id.btnOthers);

        btnClosed.setOnClickListener(this);
        btnOpen.setOnClickListener(this);
        btnOther.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btnClosed:
                intent(1);
                break;
            case R.id.btnOpen:
                intent(2);
                break;
            case R.id.btnOthers:
                intent(3);
                break;
        }
    }

    private void intent(int type){
        Intent intent = new Intent(this, MainHospitalActivity.class);
        intent.putExtra(Constant.CHOOSE_TYPE, type);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_history:
                Intent intent = new Intent(this, HistoryOrderActivity.class);
                startActivity(intent);
                break;
            case R.id.action_logout:
                showLogout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showLogout(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);

        TextView btnOk = (TextView)dialog.findViewById(R.id.btn_positive);
        TextView btnCancel = (TextView)dialog.findViewById(R.id.btn_negative);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceUtil.setToken(SelectBookActivity.this, "");
                PreferenceUtil.clearData(SelectBookActivity.this);
                startActivity(new Intent(SelectBookActivity.this, ChooseActivity.class));
                finish();
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
