package azstack.com.optimime.clinic.ui.model;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import azstack.com.optimime.clinic.ui.ui.ChooseActivity;


/**
 * Created by Dang Luu on 03/08/2016.
 */
public class Result {
    private boolean success;
    private int total;
    private String login_token;
    private Object data;
    private String message;
    private Context context;


    public boolean isSuccess() {
        if (success == true) {
            return true;
        } else {
            if (message.equals("Hospital Token mismatched")) {
                Intent intent = new Intent(context, ChooseActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public Result(Context context, boolean success, String login_token, Object data, String message, int total) {
        this.success = success;
        this.login_token = login_token;
        this.data = data;
        this.message = message;
        this.context = context;
        this.total = total;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getLogin_token() {
        return login_token;
    }

    public void setLogin_token(String login_token) {
        this.login_token = login_token;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
