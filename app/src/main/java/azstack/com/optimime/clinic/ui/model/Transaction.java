package azstack.com.optimime.clinic.ui.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Dang Luu on 2/25/2017.
 */

public class Transaction implements Parcelable{
    private long id, patient_id,clinic_id, hospital_id;
    private int status, type;
    private String hospital_department, consultant_name, created_at, note;
    private String booking_time, mri_cost, scanned_invoice, mri_cost_currency;
    private String filePathForm, filePathInvoice;
    private Patient patient;
    private Clinic clinic;
    private Hospital hospital;
    private int hideTime;

    public Transaction() {
    }

    protected Transaction(Parcel in) {
        id = in.readLong();
        patient_id = in.readLong();
        clinic_id = in.readLong();
        hospital_id = in.readLong();
        status = in.readInt();
        type = in.readInt();
        hospital_department = in.readString();
        consultant_name = in.readString();
        created_at = in.readString();
        booking_time = in.readString();
        patient = in.readParcelable(Patient.class.getClassLoader());
        clinic = in.readParcelable(Clinic.class.getClassLoader());
        hospital = in.readParcelable(Hospital.class.getClassLoader());
        hideTime = in.readInt();
        note = in.readString();
        mri_cost = in.readString();
        scanned_invoice = in.readString();
        mri_cost_currency = in.readString();
        filePathForm = in.readString();
        filePathInvoice = in.readString();
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public String getFilePathForm() {
        return filePathForm;
    }

    public void setFilePathForm(String filePathForm) {
        this.filePathForm = filePathForm;
    }

    public String getFilePathInvoice() {
        return filePathInvoice;
    }

    public void setFilePathInvoice(String filePathInvoice) {
        this.filePathInvoice = filePathInvoice;
    }

    public String getHospital_department() {
        return hospital_department;
    }

    public void setHospital_department(String hospital_department) {
        this.hospital_department = hospital_department;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setMri_cost(String mri_cost) {
        this.mri_cost = mri_cost;
    }

    public void setScanned_invoice(String scanned_invoice) {
        this.scanned_invoice = scanned_invoice;
    }

    public void setMri_cost_currency(String mri_cost_currency) {
        this.mri_cost_currency = mri_cost_currency;
    }

    public Clinic getClinic() {
        return clinic;
    }

    public void setClinic(Clinic clinic) {
        this.clinic = clinic;
    }

    public String getNote() {
        return note;
    }

    public String getMri_cost() {
        return mri_cost;
    }

    public String getScanned_invoice() {
        return scanned_invoice;
    }

    public String getMri_cost_currency() {
        return mri_cost_currency;
    }

    public int getHideTime() {
        return hideTime;
    }

    public void setHideTime(int hideTime) {
        this.hideTime = hideTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getBooking_time() {
        return booking_time;
    }

    public void setBooking_time(String booking_time) {
        this.booking_time = booking_time;
    }

    public String getConsultant_name() {
        return consultant_name;
    }

    public void setConsultant_name(String consultant_name) {
        this.consultant_name = consultant_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(long patient_id) {
        this.patient_id = patient_id;
    }

    public long getClinic_id() {
        return clinic_id;
    }

    public void setClinic_id(long clinic_id) {
        this.clinic_id = clinic_id;
    }

    public long getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(long hospital_id) {
        this.hospital_id = hospital_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(patient_id);
        dest.writeLong(clinic_id);
        dest.writeLong(hospital_id);
        dest.writeInt(status);
        dest.writeInt(type);
        dest.writeString(hospital_department);
        dest.writeString(consultant_name);
        dest.writeString(created_at);
        dest.writeString(booking_time);
        dest.writeParcelable(patient, flags);
        dest.writeParcelable(clinic, flags);
        dest.writeParcelable(hospital, flags);
        dest.writeInt(hideTime);
        dest.writeString(note);
        dest.writeString(mri_cost);
        dest.writeString(scanned_invoice);
        dest.writeString(mri_cost_currency);
        dest.writeString(filePathForm);
        dest.writeString(filePathInvoice);
    }
}
