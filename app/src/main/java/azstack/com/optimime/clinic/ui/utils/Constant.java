package azstack.com.optimime.clinic.ui.utils;

/**
 * Created by Dang Luu on 2/16/2017.
 */

public class Constant {
    public static final String PREF_STATUS_APP = "PREF_STATUS_APP";
    public static final String FOLDER_NAME = "Optimimed_Hospital";
    public static final String FOLDER_NAME_IMAGE = "Image";

    public static final String CHOOSE_TYPE = "type";
    public static final int PUBLIC_HOSPITAL = 0;
    public static final int PRIVATE_CLINIC = 1;
    public static final int FROM_HISTORY = 0;
    public static final int FROM_CALENDAR = 1;

    public static final String CLINIC = "clinic";
    public static final String PATH = "path_photo";
    public static final String BASE64 = "base64";
    public static final String BASE64INVOICE = "base64invoice";
    public static final String NAME = "name";
    public static final String CONTACT = "contact";
    public static final String CALENDAR = "calendar";
    public static final String DATE = "date";
    public static final String TIME = "time";
    public static final String TOKEN = "token";
    public static final String TIME_KEY = "time_key";
    public static final String CLINIC_ID = "clinic_id";
    public static final String RETURN_DATA = "return_data";
    public static final String SLOT = "available_slot";
    public static final String ID = "id";
    public static final String PATIENT_ID = "patient_id";
    public static final String NOTE = "note";
    public static final String STATUS = "status";
    public static final String POSITION = "position";
    public static final String MORE_DETAIL = "more_detail";
    public static final String BOOKING_TIME = "booking_time";
    public static final String ORDER_BY = "order_by";
    public static final String IDENTIFY_CARD = "identify_card";
    public static final String DEPARTMENT = "hospital_department";
    public static final String CONSULTANT = "consultant_name";
    public static final String TRANSACTION = "transaction";
    public static final String MRI_COST = "mri_cost";
    public static final String REMARK = "remark";
    public static final String MODE = "mode";
    public static final String CHANGE_FORM = "scanned_profile";
    public static final String CHANGE_INVOICE = "scanned_invoice";

    public static final String URL_BASE = "http://103.56.156.178:10002/api/";
    public static final String URL_CATEGORY_BROWSER = URL_BASE + "category/browse";
    public static final String URL_USER_HOSPITAL_REGISTER = URL_BASE + "user/hospitalRegister";
    public static final String URL_USER_HOSPITAL_UPDATE = URL_BASE + "hospitalProfile/update";
    public static final String URL_USER_HOSPITAL_LOGIN = URL_BASE + "user/loginByCode";
    public static final String URL_CLINNIC_BROWSER = URL_BASE + "clinicProfile/browse";
    public static final String URL_CLINNIC_GET = URL_BASE + "clinicProfile/get";
    public static final String URL_PATIENT_TRANSACTION_BROWSE = URL_BASE + "patientClinicTransaction/browse";
//    Hospital
    public static final String URL_HOSPITAL_CALENDAR = URL_BASE + "clinicSlotSetting/getSchedule";
    public static final String URL_PATIENT_CREAT = URL_BASE + "patientProfile/create";
    public static final String URL_PATIENT_TRANSACTION = URL_BASE + "patientClinicTransaction/create";
//    Clinic
    public static final String URL_CLINIC_CALENDAR = URL_BASE + "clinicSlotSetting/browse";
    public static final String URL_CREATE_SLOT = URL_BASE + "clinicSlotSetting/create";
    public static final String URL_UPDATE_SLOT = URL_BASE + "clinicSlotSetting/update";
    public static final String URL_GET_TRANSACTION = URL_BASE + "patientClinicTransaction/browse";
    public static final String URL_UPDATE_TRANSACTION = URL_BASE + "patientClinicTransaction/update";
    public static final String URL_GET_PATIENT_DETAIL = URL_BASE + "patientProfile/get";
    // Update
    public static final String URL_PATIENT_UPDATE = URL_BASE + "patientProfile/update";
    public static final String URL_TRANSACTION_UPDATE = URL_BASE + "patientClinicTransaction/update";
    // Share
    public static final String URL_SHARE_BOOKING = URL_BASE + "notification/emailTransaction";

    public static String status = "status";
    public static String login_token = "login_token";
    public static String message = "message";
    public static String token = "token";
    public static String avatar = "avatar";
    public static String id = "id";
    public static String success = "success";
    public static String latitude = "latitude";
    public static String patient = "patient";
    public static String transaction = "transaction";
    public static String type = "type";

    public static String longitude = "longitude";
    public static String device_id = "device_id";
    public static String password = "password";
    public static String username = "username";
    public static String data = "data";
    public static String description = "description";
    public static String address = "address";
    public static String base64_scanned_profile = "base64_scanned_profile";
    public static String base64_scanned_profile_extension = "base64_scanned_profile_extension";
    public static String my_id = "my_id";
    public static String latlon = "latlon";
    public static String patient_id = "patient_id";
    public static String clinic_name = "clinic_name";
    public static final String hospital_id = "hospital_id";
    public static String more_detail = "more_detail";
    public static String fullname = "fullname";
    public static String category_id = "category_id";
    public static String category_name = "category_name";
    public static String phone = "phone";
    public static String search = "ssearch";
}
