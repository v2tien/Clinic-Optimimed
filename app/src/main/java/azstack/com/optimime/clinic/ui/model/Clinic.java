package azstack.com.optimime.clinic.ui.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Dang Luu on 2/16/2017.
 */

public class Clinic implements Parcelable{

    private int id;
    private int uid;
    private int current_slot;
    private double latitude, longitude;
    private String name;
    private String description;
    private String address;
    private String category_id;
    private String phone;
    private String fullname;

    public Clinic() {
    }

    public Clinic(double lat, double longitude) {
        this.latitude = lat;
        this.longitude = longitude;
    }

    protected Clinic(Parcel in) {
        id = in.readInt();
        uid = in.readInt();
        current_slot = in.readInt();
        latitude = in.readDouble();
        longitude = in.readDouble();
        name = in.readString();
        description = in.readString();
        address = in.readString();
        category_id = in.readString();
        phone = in.readString();
        fullname = in.readString();
    }

    public static final Creator<Clinic> CREATOR = new Creator<Clinic>() {
        @Override
        public Clinic createFromParcel(Parcel in) {
            return new Clinic(in);
        }

        @Override
        public Clinic[] newArray(int size) {
            return new Clinic[size];
        }
    };

    public String getFullName() {
        return fullname;
    }

    public String getCategory_id() {
        return category_id;
    }

    public String getPhone() {
        return phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getCurrent_slot() {
        return current_slot;
    }

    public void setCurrent_slot(int current_slot) {
        this.current_slot = current_slot;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(uid);
        dest.writeInt(current_slot);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(address);
        dest.writeString(category_id);
        dest.writeString(phone);
        dest.writeString(fullname);
    }
}
