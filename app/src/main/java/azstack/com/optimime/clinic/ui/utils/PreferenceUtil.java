package azstack.com.optimime.clinic.ui.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


/**
 * Created by Dang Luu on 09/07/2016.
 */
public class PreferenceUtil {
    public static SharedPreferences preferences;
    public static SharedPreferences.Editor editor;


    public static SharedPreferences getInstancePreference(Context context) {
        if (preferences == null) {
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return preferences;
    }

    public static SharedPreferences.Editor getInstanceEditor(Context context) {
        if (editor == null) {
            editor = getInstancePreference(context).edit();
        }
        return editor;
    }

    public static String getToken(Context context) {
        return PreferenceUtil.getInstancePreference(context).getString(Constant.TOKEN, "");
    }

    public static String getName(Context context) {
        return PreferenceUtil.getInstancePreference(context).getString(Constant.NAME, "");
    }

    public static String getAvatar(Context context) {
        return PreferenceUtil.getInstancePreference(context).getString(Constant.avatar, "");
    }

    public static long getUserId(Context context) {
        return PreferenceUtil.getInstancePreference(context).getLong(Constant.my_id, 0);
    }

    public static double getLat(Context context) {
        return PreferenceUtil.getInstancePreference(context).getFloat(Constant.latitude, 0);
    }

    public static void setLat(Context context, double latitude) {
        PreferenceUtil.getInstanceEditor(context).putFloat(Constant.latitude, (float) latitude).commit();
    }

    public static long getMyId(Context context) {
        return PreferenceUtil.getInstancePreference(context).getLong(Constant.my_id, 0);
    }

    public static void setMyId(Context context, long my_id) {
        PreferenceUtil.getInstanceEditor(context).putLong(Constant.my_id, my_id).commit();
    }

    public static int getType(Context context) {
        return PreferenceUtil.getInstancePreference(context).getInt(Constant.CHOOSE_TYPE, Constant.PUBLIC_HOSPITAL);
    }

    public static void setType(Context context, int type) {
        PreferenceUtil.getInstanceEditor(context).putInt(Constant.CHOOSE_TYPE, type).commit();
    }


    public static double getLon(Context context) {
        return PreferenceUtil.getInstancePreference(context).getFloat(Constant.longitude, 0);
    }

    public static void setlon(Context context, double longitude) {
        PreferenceUtil.getInstanceEditor(context).putFloat(Constant.longitude, (float) longitude).commit();
    }

    public static void setToken(Context context, String token) {
        PreferenceUtil.getInstanceEditor(context).putString(Constant.TOKEN, token).commit();
    }

    public static String getTimeKey(Context context) {
        return PreferenceUtil.getInstancePreference(context).getString(Constant.TIME_KEY, "");
    }

    public static void setTimeKey(Context context, String time) {
        PreferenceUtil.getInstanceEditor(context).putString(Constant.TIME_KEY, time).commit();
    }

    public static String getBase64Image(Context context) {
        return PreferenceUtil.getInstancePreference(context).getString(Constant.BASE64, "");
    }

    public static void setBase64Image(Context context, String base64) {
        PreferenceUtil.getInstanceEditor(context).putString(Constant.BASE64, base64).commit();
    }

    public static void setBase64Invoice(Context context, String base64) {
        PreferenceUtil.getInstanceEditor(context).putString(Constant.BASE64INVOICE, base64).commit();
    }

    public static String getBase64Invoice(Context context) {
        return PreferenceUtil.getInstancePreference(context).getString(Constant.BASE64INVOICE, "");
    }

    public static void setName(Context context, String name) {
        PreferenceUtil.getInstanceEditor(context).putString(Constant.NAME, name).commit();
    }

    public static void setStatusApp(Context context, boolean inForeground) {
        PreferenceUtil.getInstanceEditor(context).putBoolean(Constant.PREF_STATUS_APP, inForeground);
        PreferenceUtil.getInstanceEditor(context).commit();
    }

    public static boolean appInForeGround(Context context) {
        return PreferenceUtil.getInstancePreference(context).getBoolean(Constant.PREF_STATUS_APP, false);
    }

    public static void clearData(Context context) {
        SharedPreferences.Editor editor = getInstancePreference(context).edit();
        editor.clear();
        editor.commit();
    }

}
