package azstack.com.optimime.clinic.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.model.Transaction;
import azstack.com.optimime.clinic.ui.ui.BookingActivity;
import azstack.com.optimime.clinic.ui.ui.PatientDetailActivity;
import azstack.com.optimime.clinic.ui.utils.Constant;
import azstack.com.optimime.clinic.ui.utils.Utils;


public class ListClinicAdapter extends RecyclerView.Adapter<ListClinicAdapter.ClinicHolder> {

    private List<Transaction> transactions;
    private Context context;
    private int type = 0;

    public ListClinicAdapter(Context context, List<Transaction> transactions, int type) {
        this.transactions = transactions;
        this.context = context;
        this.type = type;
    }

    public void setList(List<Transaction> transactions) {
        this.transactions = transactions;
        notifyDataSetChanged();
    }

    @Override
    public ClinicHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_clinic, parent, false);
        ClinicHolder rcv = new ClinicHolder(view, new ViewHolderClicks() {
            @Override
            public void clickPlace(int position) {
                if (type == 0) {
                    Intent intent = new Intent(context, PatientDetailActivity.class);
                    intent.putExtra(Constant.POSITION, position);
                    intent.putExtra(Constant.TRANSACTION, transactions.get(position));
                    intent.putExtra(Constant.CHOOSE_TYPE, 0);
                    context.startActivity(intent);
                } else if (type == 1) {
                    Intent intent = new Intent(context, BookingActivity.class);
                    intent.putExtra(Constant.TRANSACTION, transactions.get(position));
                    intent.putExtra(Constant.MODE, Constant.FROM_HISTORY);
                    context.startActivity(intent);
                }
            }
        });
        return rcv;
    }

    @Override
    public void onBindViewHolder(ClinicHolder holder, final int position) {
        ClinicHolder clinicHolder = (ClinicHolder) holder;
        final Transaction transaction = transactions.get(position);
        clinicHolder.tvName.setText(transaction.getPatient().getFullname());
        if (transaction.getHospital() == null)
            clinicHolder.tvNameHospital.setText(transaction.getClinic().getFullName());
        else
            clinicHolder.tvNameHospital.setText(transaction.getHospital().getName());
        clinicHolder.tvRef.setText("REF: 000" + transaction.getId());
        if (transaction.getStatus() == 1)
            clinicHolder.tvStatus.setText("(" + context.getString(R.string.lbl_transferred) + ")");
//        if(transaction.getStatus() == 2)
//            clinicHolder.tvStatus.setText("(" + transaction.getStatus() + ")");
        if (transaction.getStatus() == 3)
            clinicHolder.tvStatus.setText("(" + context.getString(R.string.lbl_arrived) + ")");
//        if (transaction.getStatus() == 4)
//            clinicHolder.tvStatus.setText("(" + context.getString(R.string.lbl_transferred) + ")");
        if (transaction.getStatus() == 5)
            clinicHolder.tvStatus.setText("(" + context.getString(R.string.lbl_no_show) + ")");
        if (transaction.getStatus() == 6)
            clinicHolder.tvStatus.setText("(" + context.getString(R.string.successfully) + ")");
        if (transaction.getStatus() == 7)
            clinicHolder.tvStatus.setText("(" + context.getString(R.string.lbl_unsuccessfully) + ")");

//        String date = transaction.getBooking_time().substring(1, 8);
        String dateTime = Utils.DateTime(context, transaction.getBooking_time());
        if (transaction.getHideTime() == 1) {
            clinicHolder.tvDateTime.setVisibility(View.GONE);
        } else {
            clinicHolder.tvDateTime.setVisibility(View.VISIBLE);
            clinicHolder.tvDateTime.setText(dateTime);
        }

        String time = transaction.getBooking_time().substring(8, 10);
        if (Integer.parseInt(time) < 12) {
            time = time + ":00 AM";
        } else {
            time = time + ":00 PM";
        }
        clinicHolder.tvTime.setText(time);
//        oldTime = date;
        if (type == 0) {
            clinicHolder.tvStatus.setVisibility(View.VISIBLE);
            clinicHolder.imvEdit.setVisibility(View.VISIBLE);
            clinicHolder.imvEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, PatientDetailActivity.class);
                    intent.putExtra(Constant.POSITION, position);
                    intent.putExtra(Constant.TRANSACTION, transactions.get(position));
                    intent.putExtra(Constant.CHOOSE_TYPE, 1);
                    context.startActivity(intent);
                }
            });
        } else if (type == 1) {
            clinicHolder.imvEdit.setVisibility(View.GONE);
            clinicHolder.tvStatus.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }


    public class ClinicHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName, tvDateTime, tvTime, tvRef, tvStatus, tvNameHospital;
        ImageView imvEdit;
        public ViewHolderClicks holderClicks;

        public ClinicHolder(View itemView, ViewHolderClicks holderClicks) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvDateTime = (TextView) itemView.findViewById(R.id.tvDateTime);
            tvTime = (TextView) itemView.findViewById(R.id.tvTime);
            tvRef = (TextView) itemView.findViewById(R.id.tvRef);
            tvStatus = (TextView) itemView.findViewById(R.id.tvStatus);
            tvNameHospital = (TextView) itemView.findViewById(R.id.tvNameHospital);
            imvEdit = (ImageView) itemView.findViewById(R.id.imvEdit);

            this.holderClicks = holderClicks;
            itemView.setOnClickListener(this);
//            imvEdit.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (holderClicks != null) {
                // listener lick each item place
                holderClicks.clickPlace(getLayoutPosition());
            }
        }
    }

    public interface ViewHolderClicks {
        void clickPlace(int position);
    }
}
