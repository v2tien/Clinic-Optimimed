package azstack.com.optimime.clinic.ui.model;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;

import com.alamkanak.weekview.WeekViewEvent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by VuVan on 05/04/2017.
 */

public class CalendarClinic implements Parcelable {
    private int id;
    private int clinic_id;
    private int available_slot;
    private String time_key;
    private String created_at;
    private String modified_at;
    private String clinic_service_ids;

    public CalendarClinic() {
    }

    protected CalendarClinic(Parcel in) {
        id = in.readInt();
        clinic_id = in.readInt();
        clinic_service_ids = in.readString();
        available_slot = in.readInt();
        time_key = in.readString();
        created_at = in.readString();
        modified_at = in.readString();
    }

    public static final Creator<CalendarClinic> CREATOR = new Creator<CalendarClinic>() {
        @Override
        public CalendarClinic createFromParcel(Parcel in) {
            return new CalendarClinic(in);
        }

        @Override
        public CalendarClinic[] newArray(int size) {
            return new CalendarClinic[size];
        }
    };

    public String getType() {
        return clinic_service_ids;
    }

    public void setType(String type) {
        this.clinic_service_ids = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClinic_id() {
        return clinic_id;
    }

    public void setClinic_id(int clinic_id) {
        this.clinic_id = clinic_id;
    }

    public int getAvailable_slot() {
        return available_slot;
    }

    public void setAvailable_slot(int available_slot) {
        this.available_slot = available_slot;
    }

    public String getTime_key() {
        return time_key;
    }

    public void setTime_key(String time_key) {
        this.time_key = time_key;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getModified_at() {
        return modified_at;
    }

    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(clinic_id);
        dest.writeString(clinic_service_ids);
        dest.writeInt(available_slot);
        dest.writeString(time_key);
        dest.writeString(created_at);
        dest.writeString(modified_at);
    }

    @SuppressLint("SimpleDateFormat")
    public WeekViewEvent toWeekViewEvent() {

        // Parse time.
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date start = new Date();
        Date end = new Date();
        int time = Integer.parseInt(getTime_key().substring(8, 10));
        String timeStart = time + ":00";
        String timeEnd = time + 1 + ":00";
        try {
            start = sdf.parse(timeStart);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            end = sdf.parse(timeEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Initialize start and end time.
        Calendar now = Calendar.getInstance();
        Calendar startTime = (Calendar) now.clone();
        startTime.setTimeInMillis(start.getTime());
        startTime.set(Calendar.YEAR, now.get(Calendar.YEAR));
        startTime.set(Calendar.MONTH, Integer.parseInt(getTime_key().substring(4, 6)) - 1);
        startTime.set(Calendar.DAY_OF_MONTH, Integer.parseInt(getTime_key().substring(6, 8)));
        Calendar endTime = (Calendar) startTime.clone();
        endTime.setTimeInMillis(end.getTime());
        endTime.set(Calendar.YEAR, startTime.get(Calendar.YEAR));
        endTime.set(Calendar.MONTH, startTime.get(Calendar.MONTH));
        endTime.set(Calendar.DAY_OF_MONTH, startTime.get(Calendar.DAY_OF_MONTH));

        // Create an week view event.
        WeekViewEvent weekViewEvent = new WeekViewEvent();
        weekViewEvent.setId(getId());
        weekViewEvent.setName(String.valueOf(getAvailable_slot()));
        weekViewEvent.setStartTime(startTime);
        weekViewEvent.setEndTime(endTime);
        weekViewEvent.setSlot(getAvailable_slot());
        weekViewEvent.setType(getType());
        weekViewEvent.setColor(Color.parseColor("#00ffffff"));
        return weekViewEvent;
    }
}
