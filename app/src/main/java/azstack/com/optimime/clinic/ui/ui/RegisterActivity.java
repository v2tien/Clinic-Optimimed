package azstack.com.optimime.clinic.ui.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.CategorySpinAdapter;
import azstack.com.optimime.clinic.ui.model.Category;
import azstack.com.optimime.clinic.ui.model.Hospital;
import azstack.com.optimime.clinic.ui.model.Result;
import azstack.com.optimime.clinic.ui.utils.Constant;
import azstack.com.optimime.clinic.ui.utils.GPSTracker;
import azstack.com.optimime.clinic.ui.utils.HospitalRequestUtils;
import azstack.com.optimime.clinic.ui.utils.PreferenceUtil;
import azstack.com.optimime.clinic.ui.utils.VolleyUtils;


public class RegisterActivity extends BaseActivity {
    private EditText edtEmail, edtClinicName, edtPassword;
    private Spinner spinCategory;
    private Button btnRegister;
    private GPSTracker gpsTracker;
    private String email, name, password;

    private double lat = 0, lon = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setTitle(R.string.register);
        getListCategory();

        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtClinicName = (EditText) findViewById(R.id.edtClinicName);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        spinCategory = (Spinner) findViewById(R.id.spinCategory);
        btnRegister.setOnClickListener(this);

        gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            lat = gpsTracker.getLatitude();
            lon = gpsTracker.getLongitude();
        }
//        else {
//            gpsTracker.showSettingsAlert();
//        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btnRegister:
                email = edtEmail.getText().toString();
                name = edtClinicName.getText().toString();
                password = edtPassword.getText().toString();
                if (!email.isEmpty() && !name.isEmpty() && !password.isEmpty()) {
                    final ProgressDialog progressDialog = new ProgressDialog(this);
                    progressDialog.setMessage(getString(R.string.loading));
                    progressDialog.show();
                    HospitalRequestUtils.register(this, email, password, name, lat, lon, "1", new VolleyUtils.OnRequestListenner() {
                        @Override
                        public void onSussces(String response, Result result) {
                            progressDialog.dismiss();
                            if (result.isSuccess()) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    PreferenceUtil.setToken(RegisterActivity.this, jsonObject.getString(Constant.token));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Hospital hospital = (Hospital) VolleyUtils.getRegister(response, Hospital.class);
                                if (hospital != null) {
                                    PreferenceUtil.setLat(RegisterActivity.this, hospital.getLatitude());
                                    PreferenceUtil.setlon(RegisterActivity.this, hospital.getLongitude());
                                    PreferenceUtil.setMyId(RegisterActivity.this, hospital.getId());

                                }
                                startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                                finish();
                            } else {
                                Toast.makeText(RegisterActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(String error) {
                            progressDialog.dismiss();
                        }
                    });
                    break;
                } else if (email.isEmpty() && name.isEmpty() && password.isEmpty()) {
                    Toast.makeText(this, R.string.invalid_info, Toast.LENGTH_SHORT).show();
                } else if (email.isEmpty()) {
                    Toast.makeText(this, R.string.invalid_email, Toast.LENGTH_SHORT).show();
                } else if (name.isEmpty()) {
                    Toast.makeText(this, R.string.invalid_name_hospital, Toast.LENGTH_SHORT).show();
                } else if (password.isEmpty()) {
                    Toast.makeText(this, R.string.invalid_password, Toast.LENGTH_SHORT).show();
                }
        }
    }


    private void getListCategory() {
        HospitalRequestUtils.getListCategory(this, "1", new VolleyUtils.OnRequestListenner() {
            @Override
            public void onSussces(String response, Result result) {
                final List<Category> categories = VolleyUtils.getListModelFromRespone(response, Category.class);
                spinCategory.setAdapter(new CategorySpinAdapter(RegisterActivity.this, categories));
//                spinCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                        category_id = categories.get(i).getId();
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> adapterView) {
//
//                    }
//                });
            }

            @Override
            public void onError(String error) {

            }
        });
    }

}
