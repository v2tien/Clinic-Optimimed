package azstack.com.optimime.clinic.ui.model;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.alamkanak.weekview.WeekViewEvent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by VuVan on 04/04/2017.
 */

public class Event implements Parcelable {

    private String date;
    private String start_time;
    private String end_time;
    private Clinic clinic;
    private int color;

    protected Event(Parcel in) {
        date = in.readString();
        start_time = in.readString();
        end_time = in.readString();
        clinic = in.readParcelable(Clinic.class.getClassLoader());
        color = in.readInt();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public Event() {
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getDate() {
        return date;
    }

    public String getStart_time() {
        return start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public Clinic getClinic() {
        return clinic;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public void setClinic(Clinic clinic) {
        this.clinic = clinic;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeString(start_time);
        dest.writeString(end_time);
        dest.writeParcelable(clinic, flags);
        dest.writeInt(color);
    }

    @SuppressLint("SimpleDateFormat")
    public WeekViewEvent toWeekViewEvent() {

        // Parse time.
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date start = new Date();
        Date end = new Date();
//        int hourStart = Integer.parseInt(getStart_time().substring(8, 10));
        try {
            start = sdf.parse(getStart_time());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            end = sdf.parse(getEnd_time());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Initialize start and end time.
        Calendar now = Calendar.getInstance();
        Calendar startTime = (Calendar) now.clone();
        startTime.setTimeInMillis(start.getTime());
        Log.e("start", start.getTime()+"");
        startTime.set(Calendar.YEAR, now.get(Calendar.YEAR));
        startTime.set(Calendar.MONTH, (now.get(Calendar.MONTH)));
        startTime.set(Calendar.DAY_OF_MONTH, Integer.parseInt(getDate()));

        Calendar endTime = (Calendar) startTime.clone();
        endTime.setTimeInMillis(end.getTime());
        Log.e("end", end.getTime()+"");
        endTime.set(Calendar.YEAR, startTime.get(Calendar.YEAR));
        endTime.set(Calendar.MONTH, (startTime.get(Calendar.MONTH)));
        endTime.set(Calendar.DAY_OF_MONTH, Integer.parseInt(getDate()));

        // Create an week view event.
        WeekViewEvent weekViewEvent = new WeekViewEvent();
        weekViewEvent.setId(getClinic().getId());
        weekViewEvent.setName(getClinic().getName());
        weekViewEvent.setStartTime(startTime);
        weekViewEvent.setEndTime(endTime);
        weekViewEvent.setLocation(getClinic().getLatitude() + "," + getClinic().getLongitude());
        weekViewEvent.setColor(Color.parseColor("#00ffffff"));
        weekViewEvent.setmPhone(getClinic().getPhone());
        weekViewEvent.setmAddress(getClinic().getAddress());
        weekViewEvent.setColor(getColor());

        return weekViewEvent;
    }
}
