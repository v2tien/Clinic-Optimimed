package azstack.com.optimime.clinic.ui.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.utils.Constant;

public class ChooseActivity extends BaseActivity {
    private Button btnHospital, btnClinic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);

        btnHospital = (Button) findViewById(R.id.btnHospital);
        btnClinic = (Button) findViewById(R.id.btnClinic);
        btnHospital.setOnClickListener(this);
        btnClinic.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btnHospital:
                Intent intent = new Intent(this, LoginActivity.class);
                intent.putExtra(Constant.CHOOSE_TYPE, Constant.PUBLIC_HOSPITAL);
                startActivity(intent);
                break;
            case R.id.btnClinic:
                Intent intent1 = new Intent(this, LoginActivity.class);
                intent1.putExtra(Constant.CHOOSE_TYPE, Constant.PRIVATE_CLINIC);
                startActivity(intent1);
                break;
        }
    }
}
