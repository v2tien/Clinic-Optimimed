package azstack.com.optimime.clinic.ui.service;

public enum Notify {
    DATA_PUSH("azstack.com.optimime.clinic.data.push"), UPDATE_STATUS("azstack.com.optimime.clinic.update.status");

    private String value;

    private Notify(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
