package azstack.com.optimime.clinic.ui.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.model.Result;
import azstack.com.optimime.clinic.ui.model.Transaction;


/**
 * Created by Dang Luu on 07/07/2016.
 */
public class VolleyUtils {

    private static final String TAG = "VolleyUtils";

    public interface OnRequestListenner {
        public void onSussces(String response, Result result);

        public void onError(String error);
    }

    private OnRequestListenner onRequestListenner;

    public void setOnRequestComplete(OnRequestListenner onRequestListenner) {
        this.onRequestListenner = onRequestListenner;
    }

    private HashMap<String, String> params;
    private static String url;
    private Context context;
    private RequestQueue queue;

    public VolleyUtils(Context context, String url) {
        this.context = context;
        this.url = url;
        queue = Volley.newRequestQueue(context);
        params = new HashMap<>();
    }

    public HashMap<String, String> getParams() {
        Iterator i = params.keySet().iterator();

        while (i.hasNext()) {
            String key = i.next().toString();
            String value = params.get(key);
            Log.e("params: " + key, value);
        }
        return params;
    }

    boolean isUp1s = false, isResult = false;
    ProgressDialog progressDialog = null;

    public void query(int method) {
        isUp1s = false;
        isResult = false;

        boolean isShow = false;
        if (url == null) {
            return;
        }

//        if (url.equals(Constant.LINK_LOGOUT)
//                || url.equals(Constant.LINK_UPDATE_PROFILE)
//                || url.equals(Constant.LINK_FORGOT_PASSWORD)
//                || url.equals(Constant.LINK_LOGIN_FACEBOOK)
//                || url.equals(Constant.LINK_SIGNUP)
//                || url.equals(Constant.LINK_LOGIN_EMAIL)
//                || url.equals(Constant.LINK_CHANGE_PASSWORD)) {
//            isShow = true;
//        }

        if (isShow) {
            try {
                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage(context.getString(R.string.loading));
                progressDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isUp1s = true;
                        if (isResult && isUp1s) {
                            try {
                                progressDialog.dismiss();
                            } catch (Exception e) {

                            }
                        }

                    }
                }, 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!Utils.checkNetwork(context)) {
            Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
            return;
        }

        StringRequest stringRequest = new StringRequest(method, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e(TAG, "onResponse - " + context.getClass().getSimpleName() + ": " + url);
                        Log.e(TAG, "onResponse - " + context.getClass().getSimpleName() + ": " + response);
                        isResult = true;
                        try {
                            if (isResult && isUp1s) {
                                progressDialog.dismiss();
                            }
                        } catch (Exception e) {

                        }
                        if (onRequestListenner != null) {
                            Result result = VolleyUtils.getResult(context, response);
                            try {
                                result.isSuccess();
                            } catch (Exception e) {

                            }
                            if (result != null)
                                onRequestListenner.onSussces(response, result);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + url);
                Log.e(TAG, "onErrorResponse: " + error);
                isResult = true;
                try {
                    if (isResult && isUp1s) {
                        progressDialog.dismiss();
                    }
                } catch (Exception e) {

                }
                if (onRequestListenner != null) {
                    onRequestListenner.onError(error.getMessage());
                }
            }

        }) {
            @Override
            protected Map<String, String> getParams() {

                return checkParams(params);
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }


    private Map<String, String> checkParams(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    public void addParam(String key, String values) {
        if (params == null) {
            params = new HashMap<>();
        }
        params.put(key, values);
    }


    public static final boolean requestSusscess(String responseJson) {
        try {
            JSONObject jsonObject = new JSONObject(responseJson);
            if (jsonObject.getInt("r") == 0) {
                int i = 0;
                i++;
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public static Result getResult(Context context, String responseJson) {
        Result result = null;
        try {
            JSONObject jsonObject = new JSONObject(responseJson);
            boolean success = jsonObject.getBoolean(Constant.success);
            String login_token = "";
            if (jsonObject.has(Constant.login_token)) {
                login_token = jsonObject.getString(Constant.login_token);
            }

            String message = "";
            if (jsonObject.has(Constant.message)) {
                message = jsonObject.getString(Constant.message);

            }
            int total = 0;
            if (jsonObject.has("total")) {
                total = jsonObject.getInt("total");
            }
            Object o = null;
            if (jsonObject.has(Constant.data)) {
                o = jsonObject.get("data");
            }
            result = new Result(context, success, login_token, o, message, total);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

//    public static Hospital getProfile(String response) {
//        JSONObject jsonObject = null;
//        try {
//            jsonObject = new JSONObject(response);
//            JSONObject user = jsonObject.getJSONObject("data");
//            Gson gson = new Gson();
//            return (gson.fromJson(user.toString(), Hospital.class));
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }


    public static Object getObject(String response, Class aClass) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            JSONObject user = jsonObject.getJSONObject("data");
            Gson gson = new Gson();
            return (gson.fromJson(user.toString(), aClass));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object getLoginInfo(String response, Class aClass) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            JSONObject user = jsonObject.getJSONObject("profile");
            Gson gson = new Gson();
            return (gson.fromJson(user.toString(), aClass));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object getRegister(String response, Class aClass) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            JSONObject user = jsonObject.getJSONObject("hospitalProfile");
            Gson gson = new Gson();
            return (gson.fromJson(user.toString(), aClass));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List getListModelFromRespone(String response, Class aClass) {
        List list = new ArrayList<>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            int length = jsonArray.length();
            Gson gson = new Gson();
            for (int i = 0; i < length; i++) {
                list.add(gson.fromJson(jsonArray.get(i).toString(), aClass));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List getListTransactionResponse(String response) {
        List list = new ArrayList<>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            int length = jsonArray.length();
            Gson gson = new Gson();
            String oldTime = "";
            for (int i = 0; i < length; i++) {
                Transaction transaction = gson.fromJson(jsonArray.get(i).toString(), Transaction.class);
                if (!transaction.getBooking_time().isEmpty()) {
                    String date = transaction.getBooking_time().substring(1, 8);
                    if (oldTime.equals(date)) {
                        transaction.setHideTime(1);
                    } else {
                        transaction.setHideTime(0);
                    }
                    oldTime = date;
                }
                list.add(transaction);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

}
