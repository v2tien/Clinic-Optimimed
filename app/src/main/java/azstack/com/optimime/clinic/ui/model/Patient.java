package azstack.com.optimime.clinic.ui.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Dang Luu on 2/24/2017.
 */

public class Patient implements Parcelable{
    private String fullname;
    private String address;
    private String description;
    private String phone;
    private String identify_card;
    private String scanned_profile;
    private String created_at;
    private int status;

    public Patient() {
    }

    protected Patient(Parcel in) {
        fullname = in.readString();
        address = in.readString();
        description = in.readString();
        phone = in.readString();
        identify_card = in.readString();
        scanned_profile = in.readString();
        created_at = in.readString();
        status = in.readInt();
    }

    public static final Creator<Patient> CREATOR = new Creator<Patient>() {
        @Override
        public Patient createFromParcel(Parcel in) {
            return new Patient(in);
        }

        @Override
        public Patient[] newArray(int size) {
            return new Patient[size];
        }
    };

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdentify_card() {
        return identify_card;
    }

    public void setIdentify_card(String identify_card) {
        this.identify_card = identify_card;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getScanned_profile() {
        return scanned_profile;
    }

    public void setScanned_profile(String scanned_profile) {
        this.scanned_profile = scanned_profile;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fullname);
        dest.writeString(address);
        dest.writeString(description);
        dest.writeString(phone);
        dest.writeString(identify_card);
        dest.writeString(scanned_profile);
        dest.writeString(created_at);
        dest.writeInt(status);
    }
}
