package azstack.com.optimime.clinic.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.model.Category;

/**
 * Created by Dang Luu on 2/24/2017.
 */

public class CategorySpinAdapter extends ArrayAdapter {
    private Context context;
    private List<Category> categories;

    public CategorySpinAdapter(Context context, List<Category> categories) {
        super(context, R.layout.item_spiner_category);
        this.context = context;
        this.categories = categories;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row = View.inflate(context, R.layout.item_spiner_category, null);
        TextView name = (TextView) row.findViewById(R.id.name);
        name.setText(categories.get(position).getName());
        return row;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View row = View.inflate(context, R.layout.item_spiner_category, null);
        TextView name = (TextView) row.findViewById(R.id.name);
        name.setText(categories.get(position).getName());
        return row;
    }

}
