package azstack.com.optimime.clinic.ui.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.model.Hospital;
import azstack.com.optimime.clinic.ui.model.Result;
import azstack.com.optimime.clinic.ui.utils.Constant;
import azstack.com.optimime.clinic.ui.utils.HospitalRequestUtils;
import azstack.com.optimime.clinic.ui.utils.PreferenceUtil;
import azstack.com.optimime.clinic.ui.utils.VolleyUtils;
import me.philio.pinentry.PinEntryView;

public class LoginActivity extends BaseActivity {
    private Button btnLogin;
    private TextView tvTitleCode;
    private PinEntryView pinEntryView;
    private int mType;
    private String login_as = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mType = getIntent().getIntExtra(Constant.CHOOSE_TYPE, Constant.PUBLIC_HOSPITAL);

        initComponent();
        initListener();
    }

    private void initComponent() {
        pinEntryView = (PinEntryView) findViewById(R.id.pin_entry_simple);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        tvTitleCode = (TextView) findViewById(R.id.tvTitleCode);
        btnLogin.setOnClickListener(this);
        if (mType == Constant.PRIVATE_CLINIC) {
            btnLogin.setBackgroundResource(R.drawable.bg_btn_clinic);
            tvTitleCode.setText(R.string.lbl_clinic_code);
            login_as = "clinic";
        }else {
            login_as = "hospital";
        }
    }

    private void initListener() {
        pinEntryView.setOnPinEnteredListener(new PinEntryView.OnPinEnteredListener() {
            @Override
            public void onPinEntered() {
                login(pinEntryView.getText().toString(), mType);
            }
        });
    }


    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btnLogin:
                login(pinEntryView.getText().toString(), mType);
                break;
        }
    }

    private void login(String code, final int type) {
        if (code == null || code.isEmpty() || code.length() < 4) {
            Toast.makeText(this, "Your code incorrect. Please try again!", Toast.LENGTH_SHORT).show();
            return;
        }
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        HospitalRequestUtils.signIn(this, code, FirebaseInstanceId.getInstance().getToken(), login_as,
                new VolleyUtils.OnRequestListenner() {
            @Override
            public void onSussces(String response, Result result) {
                progressDialog.dismiss();
                if (result.isSuccess()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        PreferenceUtil.setToken(LoginActivity.this, jsonObject.getString(Constant.token));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Hospital hospital = (Hospital) VolleyUtils.getLoginInfo(response, Hospital.class);
                    if (hospital != null) {
                        PreferenceUtil.setLat(LoginActivity.this, hospital.getLatitude());
                        PreferenceUtil.setlon(LoginActivity.this, hospital.getLongitude());
                        PreferenceUtil.setMyId(LoginActivity.this, hospital.getId());
                        PreferenceUtil.setName(LoginActivity.this, hospital.getFullname());
                    }
                    PreferenceUtil.setType(LoginActivity.this, type);
                    Intent intent;
                    if (type == Constant.PUBLIC_HOSPITAL) {
                        intent = new Intent(LoginActivity.this, SelectBookActivity.class);
                    } else {
                        intent = new Intent(LoginActivity.this, MainClinicActivity.class);
                    }
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
//                else {
//                    Toast.makeText(LoginActivity.this, "Your code incorrect. Please try again!", Toast.LENGTH_SHORT).show();
//                }
            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
