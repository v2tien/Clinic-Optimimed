package azstack.com.optimime.clinic.ui.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.utils.Constant;
import azstack.com.optimime.clinic.ui.utils.PreferenceUtil;

public class SplashActivity extends AppCompatActivity {

    private String token = "";
    private int mType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        token = PreferenceUtil.getToken(this);
        mType = PreferenceUtil.getType(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (token == null || token.length() < 1) {
                    startActivity(new Intent(SplashActivity.this, ChooseActivity.class));
                } else {
                    if (mType == Constant.PUBLIC_HOSPITAL) {
                        startActivity(new Intent(SplashActivity.this, SelectBookActivity.class));
                    } else {
                        startActivity(new Intent(SplashActivity.this, MainClinicActivity.class));
                    }
                }
                finish();
            }
        }, 2000);
    }
}
