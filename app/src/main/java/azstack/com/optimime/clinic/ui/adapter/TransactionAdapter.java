package azstack.com.optimime.clinic.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.model.Transaction;


public class TransactionAdapter extends RecyclerView.Adapter {

    private List<Transaction> transactions;
    private Context context;


    public TransactionAdapter(Context context, List<Transaction> transactions) {
        this.transactions = transactions;
        this.context = context;
    }

    public void setList(List<Transaction> transactions) {
        this.transactions = transactions;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_transaction, parent, false);
        return new TransactionHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        TransactionHolder transactionHolder = (TransactionHolder) holder;
        final Transaction transaction = transactions.get(position);
        transactionHolder.tvName.setText(transaction.getPatient().getFullname());
        transactionHolder.tvDate.setText(transaction.getPatient().getCreated_at());
        if(transaction.getPatient().getPhone() != null) {
            transactionHolder.tvPhone.setText(transaction.getPatient().getPhone());
        }else{
            transactionHolder.tvPhone.setText(R.string.unknown);
        }
        if(transaction.getPatient().getIdentify_card() != null) {
            transactionHolder.tvICCard.setText(transaction.getPatient().getIdentify_card());
        }else{
            transactionHolder.tvICCard.setText(R.string.unknown);
        }

        Glide.with(context)
                .load(transaction.getPatient().getScanned_profile())
                .dontAnimate().into(transactionHolder.imvPhoto);

        transactionHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(context, ClinicInfoActivity.class);
//                intent.putExtra(Constant.transaction, transaction);
//                intent.putExtra(Constant.type, 2);
//                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }


    public class TransactionHolder extends RecyclerView.ViewHolder {
        LinearLayout item;
        TextView tvName, tvDate, tvPhone, tvICCard;
        ImageView imvPhoto;

        public TransactionHolder(View itemView) {
            super(itemView);
            item = (LinearLayout) itemView.findViewById(R.id.item);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            tvPhone = (TextView) itemView.findViewById(R.id.tvPhone);
            tvICCard = (TextView) itemView.findViewById(R.id.tvICCard);
            imvPhoto = (ImageView) itemView.findViewById(R.id.imvPatient);
        }
    }


}
