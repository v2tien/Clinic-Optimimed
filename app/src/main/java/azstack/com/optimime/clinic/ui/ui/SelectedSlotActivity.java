package azstack.com.optimime.clinic.ui.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alamkanak.weekview.WeekViewEvent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.model.Clinic;
import azstack.com.optimime.clinic.ui.model.Result;
import azstack.com.optimime.clinic.ui.model.Transaction;
import azstack.com.optimime.clinic.ui.utils.Constant;
import azstack.com.optimime.clinic.ui.utils.HospitalRequestUtils;
import azstack.com.optimime.clinic.ui.utils.PreferenceUtil;
import azstack.com.optimime.clinic.ui.utils.VolleyUtils;

/**
 * Created by VuVan on 25/03/2017.
 */

public class SelectedSlotActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnBook, btnShare;
    private TextView tvDateTime, tvTime, tvAddress, tvTel, tvLocation, tvNotify;
    private MapView mMap;
    private GoogleMap mGoogleMap;

    private String date, time, patientName, contact, department, consultant, icCard;
    private WeekViewEvent event;
    private Clinic clinic;
    private Transaction transaction;
    private int type, mode, state = 0;
    private boolean isChangeForm = false, isChangeInvoice = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_slot);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mode = extras.getInt(Constant.MODE, Constant.FROM_CALENDAR);
            date = extras.getString(Constant.DATE, "");
            time = extras.getString(Constant.TIME, "");
            transaction = extras.getParcelable(Constant.TRANSACTION);
            isChangeForm = extras.getBoolean(Constant.CHANGE_FORM, false);
            isChangeInvoice = extras.getBoolean(Constant.CHANGE_INVOICE, false);
            if (mode == Constant.FROM_CALENDAR) {
                event = extras.getParcelable(Constant.CLINIC);
            } else {
                clinic = extras.getParcelable(Constant.CLINIC);
            }
        }
        initComponent();
        mMap.onCreate(savedInstanceState);
        // map
        if (mode == Constant.FROM_CALENDAR) {
            String location = event.getLocation();
            viewMap(Double.parseDouble(location.split(",")[0]), Double.parseDouble(location.split(",")[1]));
        } else {
            viewMap(clinic.getLatitude(), clinic.getLongitude());
        }
    }

    private void initComponent() {
        getSupportActionBar().setTitle(R.string.lbl_selected);
        btnBook = (Button) findViewById(R.id.btnConfirm);
        btnBook.setOnClickListener(this);
        btnShare = (Button) findViewById(R.id.btnShare);
        btnShare.setOnClickListener(this);

        tvDateTime = (TextView) findViewById(R.id.tvDateTime);
        tvTime = (TextView) findViewById(R.id.tv_time);
        tvAddress = (TextView) findViewById(R.id.tvAddress);
        tvTel = (TextView) findViewById(R.id.tv_tel);
        tvLocation = (TextView) findViewById(R.id.tv_location);
        tvNotify = (TextView) findViewById(R.id.tvNotify);
        mMap = (MapView) findViewById(R.id.map_view);
        fillData();
    }

    private void fillData() {
        tvDateTime.setText(date);
        tvTime.setText(time);
        if (mode == Constant.FROM_CALENDAR) {
            tvLocation.setText(event.getName());
            tvAddress.setText(event.getmAddress());
            tvTel.setText(event.getmPhone());
        } else {
            tvLocation.setText(clinic.getName());
            tvAddress.setText(clinic.getAddress());
            tvTel.setText(clinic.getPhone());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (state == 0) {
                    finish();
                } else {
                    Intent intent = new Intent(SelectedSlotActivity.this, SelectBookActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnConfirm:
                if (mode == Constant.FROM_CALENDAR) {
                    requestBooking(Constant.URL_PATIENT_CREAT, Constant.URL_PATIENT_TRANSACTION, 1, event.getId());
                } else if (mode == Constant.FROM_HISTORY) {
                    requestBooking(Constant.URL_PATIENT_UPDATE, Constant.URL_TRANSACTION_UPDATE, transaction.getStatus(), clinic.getId());
                }
                break;
            case R.id.btnShare:
                final Dialog dialog = new Dialog(this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_share);
                TextView btnOk = (TextView) dialog.findViewById(R.id.btn_positive);
                TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_negative);
                final EditText edtEmail = (EditText) dialog.findViewById(R.id.edtEmail);

                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        actionShare(edtEmail.getText().toString());
                        dialog.dismiss();
                    }
                });
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
        }
    }

    private void requestBooking(String url1, final String url2, int status, final long clinicId) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        HospitalRequestUtils.creatPatient(this, url1, isChangeForm, PreferenceUtil.getToken(this), transaction.getPatient().getFullname(), "",
                "", PreferenceUtil.getBase64Image(getBaseContext()), "jpg", status + "",
                transaction.getPatient().getPhone(), transaction.getPatient().getIdentify_card(),
                transaction.getPatient_id(), new VolleyUtils.OnRequestListenner() {
                    @Override
                    public void onSussces(String response, Result result) {
                        if (result.isSuccess()) {
                            int patientId = -1;
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                patientId = jsonObject.getInt(Constant.ID);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            HospitalRequestUtils.transactionpatient(SelectedSlotActivity.this, url2,
                                    isChangeInvoice, PreferenceUtil.getToken(SelectedSlotActivity.this),
                                    patientId, clinicId, PreferenceUtil.getUserId(SelectedSlotActivity.this),
                                    1, transaction.getType(), transaction.getBooking_time(),
                                    transaction.getHospital_department(), transaction.getConsultant_name(),
                                    transaction.getMri_cost(), transaction.getNote(), PreferenceUtil.getBase64Invoice(getBaseContext()),
                                    transaction.getId(), new VolleyUtils.OnRequestListenner() {
                                        @Override
                                        public void onSussces(String response, Result result) {
                                            if (result.isSuccess()) {
                                                progressDialog.dismiss();
//                                                Toast.makeText(SelectedSlotActivity.this, R.string.successfully, Toast.LENGTH_SHORT).show();
                                                state = 1;
                                                tvNotify.setVisibility(View.VISIBLE);
                                                btnBook.setVisibility(View.GONE);
                                                btnShare.setVisibility(View.VISIBLE);
                                            } else {
                                                Toast.makeText(SelectedSlotActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onError(String error) {
                                            progressDialog.dismiss();
                                        }
                                    });
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(SelectedSlotActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        progressDialog.dismiss();
                    }
                });
    }

    private void viewMap(final double latitude, final double longitude) {
        mMap.onResume();
        // show map
        try {
            MapsInitializer.initialize(this.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMap.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mGoogleMap = mMap;
                LatLng sydney = new LatLng(latitude, longitude);
                // googleMap.setOnMyLocationChangeListener(myLocationChangeListener);
                mGoogleMap.addMarker(new MarkerOptions().position(sydney));
                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(13).build();
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });
    }

    private void actionShare(String email) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        HospitalRequestUtils.getShareBooking(this, email, transaction.getId(), new VolleyUtils.OnRequestListenner() {
            @Override
            public void onSussces(String response, Result result) {
                progressDialog.dismiss();
                if (result.isSuccess()) {
                    Intent intent = new Intent(SelectedSlotActivity.this, SelectBookActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Toast.makeText(SelectedSlotActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mMap.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMap.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMap.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMap.onLowMemory();
    }
}
