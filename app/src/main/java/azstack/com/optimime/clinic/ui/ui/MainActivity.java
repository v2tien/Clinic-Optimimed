package azstack.com.optimime.clinic.ui.ui;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.fragment.MapFragment;
import azstack.com.optimime.clinic.ui.utils.PreferenceUtil;


public class MainActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addMap();

    }

    public void addMap() {
        MapFragment mapFragment = new MapFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.content, mapFragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                showLogout();
                break;
            case R.id.action_history:
                startActivity(new Intent(this, HistoryOrderActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showLogout(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);

        TextView btnOk = (TextView)dialog.findViewById(R.id.btn_positive);
        TextView btnCancel = (TextView)dialog.findViewById(R.id.btn_negative);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceUtil.setToken(MainActivity.this, "");
                startActivity(new Intent(MainActivity.this, ChooseActivity.class));
                finish();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
