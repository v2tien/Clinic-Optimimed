package azstack.com.optimime.clinic.ui.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.model.Transaction;
import azstack.com.optimime.clinic.ui.service.NotifyService;
import azstack.com.optimime.clinic.ui.ui.PatientDetailActivity;
import azstack.com.optimime.clinic.ui.utils.Constant;
import azstack.com.optimime.clinic.ui.utils.VolleyUtils;

/**
 * Created by Dang Luu on 2/15/2017.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FirebaseMessaging";
    private int pushId = 1;
    private String fullname = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//  {patientTransaction={"data":{"note":null,"patient_id":38,"patient":{"scanned_profile":"http:\/\/103.56.156.178:10002\/upload\/58c37d73ed00d.jpg","address":"nsnsnd","phone":"7394994","description":"","created_at":"2017-03-11 11:30:43","id":38,"fullname":"ndndnd","identify_card":"73838","modified_at":"0000-00-00 00:00:00","status":1},"created_at":"2017-03-11 11:30:44","id":55,"clinic":{"uid":15,"address":"","category_id":1,"latitude":21.048526,"name":"Clinnic 3","current_slot":0,"created_at":"2017-02-24 14:10:50","id":3,"modified_at":"0000-00-00 00:00:00","longitude":105.791094},"hospital_id":3,"clinic_id":3,"hospital_department":"hbsnse","consultant_name":"bbbbb","status":1},"success":true,"id":55,"message":"Created","status":200}, type=1, notification=new patient}
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Transaction transaction = new Transaction();
        for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
            String key = entry.getKey();
            if (key.equalsIgnoreCase("patientTransaction")) {
                String value = entry.getValue();
                transaction = (Transaction) VolleyUtils.getObject(value, Transaction.class);
                fullname = transaction.getPatient().getFullname();
                NotifyService.notifyPush(this, transaction);
            }
        }

        Intent intent = new Intent(this, PatientDetailActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constant.TRANSACTION, transaction);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 100, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("You have a booking from" + " " + fullname)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(100, notificationBuilder.build());

//        NotifyService.notifyPush(this, remoteMessage.getData().);
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, remoteMessage.getData() + "");
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
}
