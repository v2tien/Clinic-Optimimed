package azstack.com.optimime.clinic.ui.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Dang Luu on 2/24/2017.
 */

public class Hospital implements Parcelable {
    private int id;
    private int uid;
    private int current_slot;
    private double latitude, longitude;
    private String name;
    private String description;
    private String address;
    private String fullname;
//    private String fullname;

    public Hospital() {
    }

    public Hospital(double lat, double longitude) {
        this.latitude = lat;
        this.longitude = longitude;
    }

    protected Hospital(Parcel in) {
        id = in.readInt();
        uid = in.readInt();
        current_slot = in.readInt();
        latitude = in.readDouble();
        longitude = in.readDouble();
        name = in.readString();
        description = in.readString();
        address = in.readString();
        fullname = in.readString();
    }

    public static final Creator<Hospital> CREATOR = new Creator<Hospital>() {
        @Override
        public Hospital createFromParcel(Parcel in) {
            return new Hospital(in);
        }

        @Override
        public Hospital[] newArray(int size) {
            return new Hospital[size];
        }
    };

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getCurrent_slot() {
        return current_slot;
    }

    public void setCurrent_slot(int current_slot) {
        this.current_slot = current_slot;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(uid);
        dest.writeInt(current_slot);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(address);
        dest.writeString(fullname);
    }
}
