package azstack.com.optimime.clinic.ui.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by VuVan on 04/04/2017.
 */

public class CalendarHospital implements Parcelable{
    private int mDayOfWeek;
    private int mDayOfMonth;
    private int mMonthOfYear;
    private int mYear;
    private int mHour;
    private int mMinutes;

    public CalendarHospital() {
    }

    protected CalendarHospital(Parcel in) {
        mDayOfWeek = in.readInt();
        mDayOfMonth = in.readInt();
        mMonthOfYear = in.readInt();
        mYear = in.readInt();
        mHour = in.readInt();
        mMinutes = in.readInt();
    }

    public static final Creator<CalendarHospital> CREATOR = new Creator<CalendarHospital>() {
        @Override
        public CalendarHospital createFromParcel(Parcel in) {
            return new CalendarHospital(in);
        }

        @Override
        public CalendarHospital[] newArray(int size) {
            return new CalendarHospital[size];
        }
    };

    public int getmDayOfWeek() {
        return mDayOfWeek;
    }

    public void setmDayOfWeek(int mDayOfWeek) {
        this.mDayOfWeek = mDayOfWeek;
    }

    public int getmDayOfMonth() {
        return mDayOfMonth;
    }

    public void setmDayOfMonth(int mDayOfMonth) {
        this.mDayOfMonth = mDayOfMonth;
    }

    public int getmMonthOfYear() {
        return mMonthOfYear;
    }

    public void setmMonthOfYear(int mMonthOfYear) {
        this.mMonthOfYear = mMonthOfYear;
    }

    public int getmYear() {
        return mYear;
    }

    public void setmYear(int mYear) {
        this.mYear = mYear;
    }

    public int getmHour() {
        return mHour;
    }

    public void setmHour(int mHour) {
        this.mHour = mHour;
    }

    public int getmMinutes() {
        return mMinutes;
    }

    public void setmMinutes(int mMinutes) {
        this.mMinutes = mMinutes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mDayOfWeek);
        dest.writeInt(mDayOfMonth);
        dest.writeInt(mMonthOfYear);
        dest.writeInt(mYear);
        dest.writeInt(mHour);
        dest.writeInt(mMinutes);
    }
}
