package azstack.com.optimime.clinic.ui.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import azstack.com.optimime.clinic.R;
import azstack.com.optimime.clinic.ui.model.CalendarHospital;
import azstack.com.optimime.clinic.ui.model.Event;
import azstack.com.optimime.clinic.ui.model.Result;
import azstack.com.optimime.clinic.ui.utils.Constant;
import azstack.com.optimime.clinic.ui.utils.HospitalRequestUtils;
import azstack.com.optimime.clinic.ui.utils.PreferenceUtil;
import azstack.com.optimime.clinic.ui.utils.Utils;
import azstack.com.optimime.clinic.ui.utils.VolleyUtils;

/**
 * Created by VuVan on 25/03/2017.
 */

public class MainHospitalActivity extends BaseActivityHasBackButton implements WeekView.EventClickListener,
        MonthLoader.MonthChangeListener, WeekView.EventLongPressListener, WeekView.EmptyViewLongPressListener {

    private WeekView mWeekView;
    private List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();
    private int type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_hospital);
        initComponent();
        initListener();
    }

    private void initComponent() {
        getSupportActionBar().setTitle(PreferenceUtil.getName(this));
        getSupportActionBar().setSubtitle("Consultation Timings");
        mWeekView = (WeekView) findViewById(R.id.weekView);
        type = getIntent().getIntExtra(Constant.CHOOSE_TYPE, 1);
    }

    private void initListener() {
        // Show a toast message about the touched event.
        mWeekView.setOnEventClickListener(this);
        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        mWeekView.setMonthChangeListener(this);
        // Set long press listener for events.
        mWeekView.setEventLongPressListener(this);
        // Set long press listener for empty view
        mWeekView.setEmptyViewLongPressListener(this);
        // Set up a date time interpreter to interpret how the date and time will be formatted in
        // the week view. This is optional.
        setupDateTimeInterpreter(false);
        getListCalendar();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem btnLogout = menu.findItem(R.id.action_logout);
        btnLogout.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_today:
                mWeekView.goToToday();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Set up a date time interpreter which will show short date values when in week view and long
     * date values otherwise.
     *
     * @param shortDate True if the date values should be short.
     */
    private void setupDateTimeInterpreter(final boolean shortDate) {
        mWeekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {
                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", Locale.getDefault());
                String weekday = weekdayNameFormat.format(date.getTime());
                SimpleDateFormat format = new SimpleDateFormat(" M/d", Locale.getDefault());
//                mWeekView.setEventMarginVertical(20);
                // All android api level do not have a standard way of getting the first letter of
                // the week day name. Hence we get the first char programmatically.
                // Details: http://stackoverflow.com/questions/16959502/get-one-letter-abbreviation-of-week-day-of-a-date-in-java#answer-16959657
                if (shortDate)
                    weekday = String.valueOf(weekday.charAt(0));
                return weekday.toUpperCase() + format.format(date.getTime());
            }

            @Override
            public String interpretTime(int hour) {
                String time = "";
                if (hour == 0) {
                    time = "0 AM";
                } else if (hour == 12) {
                    time = "12 PM";
                } else if (hour > 11) {
                    time = (hour - 12) + " PM";
                } else{
                    time = hour + " AM";
                }
                return time;
            }
        });
    }

    /**
     * CalendarHospital
     */
    protected String getEventTitle(Calendar time) {
        return String.format("Event of %02d:%02d %s/%d", time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onEmptyViewLongPress(Calendar time) {
//        Toast.makeText(this, "Empty view long pressed: " + time.get(Calendar.HOUR_OF_DAY), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect, Calendar time) {
        CalendarHospital calendarHospital = new CalendarHospital();
        calendarHospital.setmDayOfWeek(time.get(Calendar.DAY_OF_WEEK));
        calendarHospital.setmDayOfMonth(time.get(Calendar.DAY_OF_MONTH));
        calendarHospital.setmMonthOfYear((time.get(Calendar.MONTH) + 1));
        calendarHospital.setmYear(time.get(Calendar.YEAR));
        calendarHospital.setmHour(time.get(Calendar.HOUR_OF_DAY));
        calendarHospital.setmMinutes(time.get(Calendar.MINUTE));

        String timeKey = Utils.TimeKey(time);
        PreferenceUtil.setTimeKey(this, timeKey);
//        Log.e("id", event.getId() + "-" + timeKey );

        Intent intent = new Intent(this, BookingActivity.class);
        intent.putExtra(Constant.CALENDAR, calendarHospital);
        intent.putExtra(Constant.CLINIC, event);
        intent.putExtra(Constant.CHOOSE_TYPE, type);
        intent.putExtra(Constant.MODE, Constant.FROM_CALENDAR);
        startActivity(intent);
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
//        Toast.makeText(this, "Long pressed event: " + event.getName(), Toast.LENGTH_SHORT).show();
    }

    public WeekView getWeekView() {
        return mWeekView;
    }

    /**
     * Checks if an event falls into a specific year and month.
     *
     * @param event The event to check for.
     * @param year  The year.
     * @param month The month.
     * @return True if the event matches the year and month.
     */
    private boolean eventMatches(WeekViewEvent event, int year, int month) {
        return (event.getStartTime().get(Calendar.YEAR) == year && (event.getStartTime().get(Calendar.MONTH)) == month - 1)
                || (event.getEndTime().get(Calendar.YEAR) == year && (event.getEndTime().get(Calendar.MONTH)) == month - 1);
    }

    @Override
    public List<? extends WeekViewEvent> onMonthChange(final int newYear, final int newMonth) {
        // Populate the week view with some events.
        final List<WeekViewEvent> matchedEvents = new ArrayList<>();
//        Log.e("onMonthChange", "onMonthChange");
        for (WeekViewEvent event : events) {
            if (eventMatches(event, newYear, newMonth)) {
                matchedEvents.add(event);
            }
        }

        return matchedEvents;
    }

    private void getListCalendar() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        HospitalRequestUtils.getHospitalCalendar(this, PreferenceUtil.getToken(getBaseContext()), type, new VolleyUtils.OnRequestListenner() {
            @Override
            public void onSussces(String response, Result result) {
                progressDialog.dismiss();
                if (result.isSuccess()) {
                    events.clear();
                    List<Event> listEvents = VolleyUtils.getListModelFromRespone(response, Event.class);
                    int[] androidColors = getResources().getIntArray(R.array.array_colors);
                    for (Event event : listEvents) {
                        String startTime = event.getStart_time().substring(8, 10) + ":00";
                        String endTime = event.getEnd_time().substring(8, 10) + ":00";
                        event.setStart_time(startTime);
                        event.setEnd_time(endTime);

                        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
                        event.setColor(randomAndroidColor);

                        events.add(event.toWeekViewEvent());
                    }
                    getWeekView().notifyDatasetChanged();
                } else {
                    Toast.makeText(MainHospitalActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
            }
        });
    }
}
