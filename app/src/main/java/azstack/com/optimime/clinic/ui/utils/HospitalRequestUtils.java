package azstack.com.optimime.clinic.ui.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;

import java.util.ArrayList;

/**
 * Created by Dang Luu on 2/16/2017.
 */

public class HospitalRequestUtils {
    public static void signIn(Context context, String code, String device_id, String login_as,
                              VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_USER_HOSPITAL_LOGIN);
        volleyUtils.addParam("code", code);
        volleyUtils.addParam("device_id", device_id);
        volleyUtils.addParam("login_as", login_as);
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void register(Context context, String username, String password, String name, double latitude, double longitude, String device_id, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_USER_HOSPITAL_REGISTER);
        volleyUtils.addParam(Constant.username, username);
        volleyUtils.addParam(password, password);
        volleyUtils.addParam(Constant.NAME, name);
        volleyUtils.addParam(Constant.latitude, latitude + "");
        volleyUtils.addParam(Constant.longitude, longitude + "");
        volleyUtils.addParam(device_id, device_id);
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void update(Context context, String token_id, long id, double latitude, double longitude, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_USER_HOSPITAL_UPDATE);
        volleyUtils.addParam(Constant.TOKEN, token_id + "");
        volleyUtils.addParam(Constant.ID, id + "");
        volleyUtils.addParam(Constant.latitude, latitude + "");
        volleyUtils.addParam(Constant.longitude, longitude + "");
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void logOut(Context context, String token, VolleyUtils.OnRequestListenner onRequestListenner) {

    }

    public static void sendOrder(Context context, String token, VolleyUtils.OnRequestListenner onRequestListenner) {

    }

    public static void findClinic(Context context, String token_id, double latitude, double longitude, int category_id, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_CLINNIC_BROWSER);
        volleyUtils.addParam(Constant.TOKEN, token_id);
        volleyUtils.addParam(Constant.latitude, latitude + "");
        volleyUtils.addParam(Constant.longitude, longitude + "");
        volleyUtils.addParam(Constant.category_id, category_id + "");
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void getClinnicInfo(Context context, String token_id, int id, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_CLINNIC_GET);
        volleyUtils.addParam(Constant.TOKEN, token_id);
        volleyUtils.addParam(Constant.ID, id + "");
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void getListCategory(Context context, String token_id, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_CATEGORY_BROWSER);
        volleyUtils.addParam(Constant.TOKEN, token_id);
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }


    public static void creatPatient(Context context, String url, boolean isChange, String token_id, String name, String address, String description,
                                    String base64_scanned_profile, String base64_scanned_profile_extension, String status,
                                    String phone, String icCard, long id, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, url);
        volleyUtils.addParam(Constant.TOKEN, token_id);
        if (url.equalsIgnoreCase(Constant.URL_PATIENT_UPDATE)) {
            volleyUtils.addParam(Constant.ID, id + "");
        }
        volleyUtils.addParam(Constant.fullname, name);
        volleyUtils.addParam(Constant.address, address);
        volleyUtils.addParam(Constant.description, description);
        if (base64_scanned_profile != null && !base64_scanned_profile.isEmpty()) {
            if (isChange == true) {
                volleyUtils.addParam(Constant.base64_scanned_profile, base64_scanned_profile);
                volleyUtils.addParam(Constant.base64_scanned_profile_extension, base64_scanned_profile_extension);
            } else {
                volleyUtils.addParam(Constant.CHANGE_FORM, base64_scanned_profile);
            }
        }
        volleyUtils.addParam(Constant.STATUS, status);
        volleyUtils.addParam(Constant.phone, phone);
        volleyUtils.addParam(Constant.IDENTIFY_CARD, icCard);
        volleyUtils.addParam(Constant.RETURN_DATA, 1 + "");
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void transactionpatient(Context context, String url, boolean isChange, String token_id, long patient_id, long clinic_id, long hospital_id,
                                          int status, int type, String time, String department, String consultant,
                                          String mriCost, String remark, String scanned_invoice, long id,
                                          VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, url);
        volleyUtils.addParam(Constant.TOKEN, token_id);
        if (url.equalsIgnoreCase(Constant.URL_TRANSACTION_UPDATE)) {
            volleyUtils.addParam(Constant.ID, id + "");
        }
        volleyUtils.addParam(Constant.PATIENT_ID, patient_id + "");
        volleyUtils.addParam(Constant.CLINIC_ID, clinic_id + "");
        volleyUtils.addParam(Constant.hospital_id, hospital_id + "");
        volleyUtils.addParam(Constant.STATUS, status + "");
        volleyUtils.addParam(Constant.CHOOSE_TYPE, type + "");
        volleyUtils.addParam(Constant.BOOKING_TIME, time);
        volleyUtils.addParam(Constant.RETURN_DATA, 1 + "");
        volleyUtils.addParam(Constant.DEPARTMENT, department);
        volleyUtils.addParam(Constant.CONSULTANT, consultant);
        volleyUtils.addParam(Constant.MRI_COST, mriCost);
        volleyUtils.addParam(Constant.NOTE, remark);
        if (isChange == true) {
            volleyUtils.addParam("base64_scanned_invoice", scanned_invoice);
            volleyUtils.addParam("base64_scanned_invoice_extension", "jpg");
        } else {
            volleyUtils.addParam(Constant.CHANGE_INVOICE, scanned_invoice);
        }

        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void getListTransaction(Context context, String token_id, long hospitalId, int page, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_PATIENT_TRANSACTION_BROWSE);
        volleyUtils.addParam(Constant.TOKEN, token_id);
        volleyUtils.addParam(Constant.hospital_id, hospitalId + "");
        volleyUtils.addParam(Constant.more_detail, Constant.patient_id + "," + "clinic_id");
        volleyUtils.addParam(Constant.ORDER_BY, "-" + Constant.BOOKING_TIME);
        volleyUtils.addParam("page", page + "");
        volleyUtils.addParam("per_page", 30 + "");
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void getListSearchTransaction(Context context, String token_id, String textSearch,
                                                VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_PATIENT_TRANSACTION_BROWSE);
        volleyUtils.addParam(Constant.TOKEN, token_id);
        volleyUtils.addParam(Constant.MORE_DETAIL, "hospital_id," + Constant.PATIENT_ID);
        volleyUtils.addParam(Constant.search, textSearch);
        volleyUtils.addParam(Constant.ORDER_BY, Constant.BOOKING_TIME);
        volleyUtils.addParam("page", -1 + "");
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void getHospitalCalendar(Context context, String token_id, int type, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_HOSPITAL_CALENDAR);
        volleyUtils.addParam(Constant.TOKEN, token_id);
        volleyUtils.addParam(Constant.CHOOSE_TYPE, type + "");
        volleyUtils.addParam("clinic_service_id", type + "");
        volleyUtils.addParam("page", -1 + "");
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void getClinicCalendar(Context context, String token_id, long clinic_id, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_CLINIC_CALENDAR);
        volleyUtils.addParam(Constant.TOKEN, token_id);
        volleyUtils.addParam(Constant.CLINIC_ID, clinic_id + "");
        volleyUtils.addParam("page", -1 + "");
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void requestCreateSlot(Context context, String token, long clinic_id, String timeKey, String slot,
                                         ArrayList<Integer> type, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_CREATE_SLOT);
        volleyUtils.addParam(Constant.TOKEN, token);
        volleyUtils.addParam(Constant.CLINIC_ID, clinic_id + "");
        volleyUtils.addParam(Constant.TIME_KEY, timeKey);
        volleyUtils.addParam(Constant.SLOT, slot);
        volleyUtils.addParam("clinic_service_ids", type + "");
        volleyUtils.addParam(Constant.RETURN_DATA, 1 + "");
        Log.e("Haizzz", token + "\n" + clinic_id + "\n" + timeKey + "\n" + slot + "-" + type);
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void requestUpdateSlot(Context context, String token, long id, long clinic_id, String timeKey, String slot,
                                         ArrayList<Integer> type, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_UPDATE_SLOT);
        volleyUtils.addParam(Constant.TOKEN, token);
        volleyUtils.addParam("id", id + "");
        volleyUtils.addParam(Constant.CLINIC_ID, clinic_id + "");
        volleyUtils.addParam(Constant.TIME_KEY, timeKey);
        volleyUtils.addParam(Constant.SLOT, slot);
        volleyUtils.addParam("clinic_service_ids", type + "");
        volleyUtils.addParam(Constant.RETURN_DATA, 1 + "");
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void getTransaction(Context context, String token_id, long clinic_id, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_GET_TRANSACTION);
        volleyUtils.addParam(Constant.TOKEN, token_id);
        volleyUtils.addParam(Constant.CLINIC_ID, clinic_id + "");
        volleyUtils.addParam(Constant.MORE_DETAIL, "hospital_id," + Constant.PATIENT_ID);
        volleyUtils.addParam(Constant.ORDER_BY, Constant.BOOKING_TIME);
        volleyUtils.addParam("page", -1 + "");
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void getPatientDetail(Context context, String token_id, long id, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_GET_PATIENT_DETAIL);
        volleyUtils.addParam(Constant.TOKEN, token_id);
        volleyUtils.addParam(Constant.ID, id + "");
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void updateTransaction(Context context, String token_id, long id, int status,
                                         String note, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_UPDATE_TRANSACTION);
        volleyUtils.addParam(Constant.TOKEN, token_id);
        volleyUtils.addParam(Constant.ID, id + "");
        volleyUtils.addParam(Constant.STATUS, status + "");
        volleyUtils.addParam(Constant.NOTE, note);
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }

    public static void getShareBooking(Context context, String email, long id, VolleyUtils.OnRequestListenner onRequestListenner) {
        VolleyUtils volleyUtils = new VolleyUtils(context, Constant.URL_SHARE_BOOKING);
        volleyUtils.addParam("email", email);
        volleyUtils.addParam("transaction_id", id + "");
        volleyUtils.setOnRequestComplete(onRequestListenner);
        volleyUtils.query(Request.Method.POST);
    }
}
